package api.security;

public enum Role {
    USER,
    USER_PENDING,
    UPGRADED_USER,
    ADMIN
}
