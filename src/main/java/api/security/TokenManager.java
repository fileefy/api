package api.security;

import api.configs.Config;
import api.database.HibernateManager;
import api.model.RefreshToken;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;

import java.time.Instant;
import java.util.Date;

/**
 * The TokenManager class is responsible for verifying and issuing access and
 * refresh tokens.
 */
public class TokenManager {

    /**
     * Lifetime of the access token.
     */
    private static final long ACCESS_TOKEN_EXPIRATION = 3600; // 1 Hour

    /**
     * Lifetime of the refresh token, if session is a short one.
     */
    private static final long REFRESH_TOKEN_EXPIRATION = 43200; // 12 Hours

    /**
     * Lifetime of the refresh token, if session is a long one.
     */
    private static final long LONG_REFRESH_TOKEN_EXPIRATION = 604800; // 7 Days

    /**
     * Algorithm used to sign tokens.
     */
    private static Algorithm algorithm;

    /**
     * Secret token used to sign tokens.
     */
    private static String SECRET;

    /**
     * Issuer of the token (server identifier).
     */
    private static String ISSUER;

    /**
     * Represents current time, is used to create tokens at the same time.
     */
    private static Instant now;

    /**
     * Configure method should be called once, during the start of the server.
     */
    public static void configure() {
        TokenManager.SECRET = Config.getenv("JWT_SECRET");
        TokenManager.ISSUER = Config.getenv("JWT_ISSUER");
        algorithm = Algorithm.HMAC256(TokenManager.SECRET);
    }

    /**
     * Token issue based on the user's id. Should be used if user's credentials
     * have been verified.
     *
     * @param userId        ID of the user, to whom token will be given.
     * @param isSessionLong Boolean which represents if session is long or short.
     *                      Based on this boolean, refresh token's expiration time
     *                      will vary.
     * @return Returns a pair af access and refresh token.
     */
    public static Tokens issueTokens(long userId, boolean isSessionLong) {
        now = Instant.now();
        long expiration;

        if (isSessionLong) {
            expiration = LONG_REFRESH_TOKEN_EXPIRATION;
        } else {
            expiration = REFRESH_TOKEN_EXPIRATION;
        }

        String accessToken = issueAccessToken(userId);
        String refreshToken = issueRefreshToken(Date.from(now.plusSeconds(expiration)));

        HibernateManager.getRefreshTokenDao().create(
                HibernateManager.getUserDao().get(userId), refreshToken
        );

        return new Tokens(accessToken, refreshToken);
    }

    /**
     * Token issue based on the refresh token.
     *
     * @param refreshToken A valid refresh token.
     * @return Returns a pair af access and refresh token.
     * @throws Exception Exception is thrown if token is not valid or
     *                   it might have been compromised.
     */
    public static Tokens issueTokens(String refreshToken) throws Exception {
        now = Instant.now();

        // Get refresh token family
        RefreshToken r = HibernateManager.getRefreshTokenDao().get(refreshToken);

        if (r != null) {

            // If token exist in the database, validate it
            try {
                verifyRefreshToken(refreshToken);
            } catch (JWTVerificationException exception) {

                // If token has already expired, delete token family
                HibernateManager.getRefreshTokenDao().delete(r.getId());
                throw new JWTCreationException(exception.getMessage(), exception.getCause());
            }
        } else {

            // If token was not found in the active refresh tokens,
            // search for it in the rotated tokens (already used)
            for (var tokenFamily : HibernateManager.getRefreshTokenDao().getAll()) {

                // If token was already rotated and client tries to use it,
                // token family MIGHT have been compromised, thus, server deletes
                // whole token family
                if (tokenFamily.getRotatedTokens().contains(refreshToken)) {
                    HibernateManager.getRefreshTokenDao().delete(tokenFamily.getId());
                    break;
                }
            }
            throw new Exception();
        }

        // Issue new tokens
        String accessToken = issueAccessToken(r.user().getId());
        String newRefreshToken = issueRefreshToken(Date.from(JWT.decode(
                r.getToken()).getExpiresAt().toInstant().plusSeconds(10)));

        // Refresh token in the database
        r = HibernateManager.getRefreshTokenDao().addRotatedToken(r.getId(), r.getToken());
        r.setToken(newRefreshToken);
        HibernateManager.getRefreshTokenDao().update(r);

        return new Tokens(accessToken, newRefreshToken);
    }

    /**
     * Issues a valid access token.
     *
     * @param userId ID of a user to whom this token will be issued.
     * @return Returns a new valid access token.
     */
    private static String issueAccessToken(long userId) {
        try {
            return JWT.create()
                    .withIssuer(TokenManager.ISSUER)
                    .withClaim("uid", userId)
                    .withExpiresAt(Date.from(now.plusSeconds(ACCESS_TOKEN_EXPIRATION)))
                    .sign(algorithm);
        } catch (JWTCreationException exception) {
            //Invalid Signing configuration / Couldn't convert Claims.
            throw new JWTCreationException(exception.getMessage(), exception.getCause());
        }
    }

    /**
     * Issues a valid refresh token.
     *
     * @param expirationDate Date when refresh token will expire.
     * @return Returns a new valid refresh token.
     */
    private static String issueRefreshToken(Date expirationDate) {
        try {
            return JWT.create()
                    .withIssuer(TokenManager.ISSUER)
                    .withExpiresAt(expirationDate)
                    .sign(algorithm);
        } catch (JWTCreationException exception) {
            //Invalid Signing configuration / Couldn't convert Claims.
            throw new JWTCreationException(exception.getMessage(), exception.getCause());
        }
    }

    /**
     * Verifies access token.
     *
     * @param token Access token.
     * @return Returns user's id, whom this token was issued.
     */
    public static long verifyAccessToken(String token) {
        try {
            var verifier = JWT.require(algorithm)
                    .withIssuer(TokenManager.ISSUER)
                    .build();
            DecodedJWT jwt = verifier.verify(token);
            return jwt.getClaim("uid").asLong();
        } catch (JWTVerificationException exception) {
            throw new JWTVerificationException(exception.getMessage());
        }
    }

    /**
     * Verifies refresh token.
     *
     * @param token Refresh token.
     */
    private static void verifyRefreshToken(String token) {
        try {
            var verifier = JWT.require(algorithm)
                    .withIssuer(TokenManager.ISSUER)
                    .build();
            verifier.verify(token);
        } catch (JWTVerificationException exception) {
            throw new JWTVerificationException(exception.getMessage());
        }
    }

    /**
     * Deletes token family of the refresh token from the database
     *
     * @param refreshToken A valid refresh token
     * @return Returns boolean which indicates if token family was fond and deleted
     */
    public static boolean invalidate(String refreshToken) {
        try {
            RefreshToken r = HibernateManager.getRefreshTokenDao().get(refreshToken);

            if (r != null) {

                // If token is found delete its token family
                HibernateManager.getRefreshTokenDao().delete(r.getId());
                return true;
            } else {

                // If token was not found in the active refresh tokens,
                // search for it in the rotated tokens (already used)
                for (var tokenFamily : HibernateManager.getRefreshTokenDao().getAll()) {

                    // If found in rotated tokens, also delete whole family
                    if (tokenFamily.getRotatedTokens().contains(refreshToken)) {
                        HibernateManager.getRefreshTokenDao().delete(tokenFamily.getId());
                        return true;
                    }
                }
                throw new Exception();
            }
        } catch (Exception e) {

            // If refresh token was nowhere found return false
            return false;
        }
    }
}
