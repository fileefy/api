package api.security;

/**
 * The Tokens class is used to store access and refresh tokens.
 */
public class Tokens {

    public final String accessToken;
    public final String refreshToken;

    public Tokens(String accessToken, String refreshToken) {
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
    }
}
