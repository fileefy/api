package api.utilities;

import club.caliope.udc.OutputFormat;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utilities {

    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
    public static final Pattern VALID_KINDLE_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[a-zA-Z0-9._%+-]+@kindle.com$", Pattern.CASE_INSENSITIVE);

    public static String[] splitCamelCase(String camelCaseString){
        return camelCaseString.split("(?<!(^|[A-Z]))(?=[A-Z])|(?<!^)(?=[A-Z][a-z])");
    }

    public static boolean checkEmail(String email, boolean isKindle) {
        Matcher matcher;
        if (isKindle) {
            matcher = VALID_KINDLE_EMAIL_ADDRESS_REGEX.matcher(email);
        } else {
            matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(email);
        }
        return matcher.find();
    }

    public static void cleanUp(Path path) throws IOException {
        Files.delete(path);
    }

    public static String generateError(String message) {
        Gson g = new Gson();
        JsonObject error = new JsonObject();

        error.addProperty("error", message);
        return g.toJson(error);
    }

    public static String generateError(Map<String, String> dictionary) {
        Gson g = new Gson();
        JsonObject errors = new JsonObject();
        JsonObject json = new JsonObject();

        for (Map.Entry<String, String> entry : dictionary.entrySet()) {
            json.addProperty(entry.getKey(), entry.getValue());
        }

        errors.add("errors", json);
        return g.toJson(errors);
    }

    public static String decode(String encoded) throws UnsupportedEncodingException {
        return URLDecoder.decode(encoded, StandardCharsets.UTF_8);
    }

    public static String encode(String string) throws UnsupportedEncodingException {
        return URLEncoder.encode(string, StandardCharsets.UTF_8);
    }

    public static OutputFormat convertFormat(FileFormat format) {
        switch (format) {
            case EPUB:
                return OutputFormat.EPUB;
            case FB2:
                return OutputFormat.FB2;
            case MARKDOWN:
                return OutputFormat.MARKDOWN;
            default:
                return  OutputFormat.DOCX;
        }
    }

    public static <T> void updateEntity(T entity, JsonObject json) throws IllegalAccessException,
            UnsupportedEncodingException {
        Gson g = new Gson();
        for (Field field: entity.getClass().getDeclaredFields()) {
            if (json.has(field.getName())){
                field.setAccessible(true);

                switch(field.getType().getSimpleName()){
                    case "int":
                        field.set(entity, json.get(field.getName()).getAsInt());
                        break;
                    case "String":
                        field.set(entity, Utilities.decode(json.get(field.getName()).getAsString()));
                        break;
                    case "long":
                        field.set(entity, json.get(field.getName()).getAsLong());
                        break;
                    case "boolean":
                        field.set(entity, json.get(field.getName()).getAsBoolean());
                        break;
                    case "double":
                        field.set(entity, json.get(field.getName()).getAsDouble());
                        break;
                    case "byte":
                        field.set(entity, json.get(field.getName()).getAsByte());
                        break;
                    case "short":
                        field.set(entity, json.get(field.getName()).getAsShort());
                        break;
                    case "float":
                        field.set(entity, json.get(field.getName()).getAsFloat());
                        break;
                    case "Number":
                        field.set(entity, json.get(field.getName()).getAsNumber());
                        break;
                    case "BigInteger":
                        field.set(entity, json.get(field.getName()).getAsBigInteger());
                        break;
                    case "BigDecimal":
                        field.set(entity, json.get(field.getName()).getAsBigDecimal());
                        break;
                    case "LocalDateTime":
                        field.set(entity, LocalDateTime.parse(json.get(field.getName()).getAsString()));
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
