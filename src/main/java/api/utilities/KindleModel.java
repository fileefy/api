package api.utilities;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public enum KindleModel {
    KINDLE_9("Kindle 9"),
    OASIS("Oasis"),
    PAPERWHITE("Paperwhite"),
    OTHER("Other");

    private String model;

    KindleModel(String model) {
        this.model = model;
    }

    public String getModel() {
        return model;
    }

    private static final Map<String,KindleModel> ENUM_MAP;

    static {
        Map<String,KindleModel> map = new HashMap<String, KindleModel>();
        for (KindleModel instance : KindleModel.values()) {
            map.put(instance.getModel(),instance);
        }
        ENUM_MAP = Collections.unmodifiableMap(map);
    }

    public static KindleModel get (String name) {
        return ENUM_MAP.get(name);
    }
}