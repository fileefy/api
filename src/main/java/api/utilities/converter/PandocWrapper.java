package api.utilities.converter;

import club.caliope.udc.DocumentConverter;
import club.caliope.udc.InputFormat;
import club.caliope.udc.OutputFormat;
import club.caliope.udc.Settings;

import java.io.File;


/**
 * A wrapper for DocumentConverter from "universal-document-converter" package.
 */
public class PandocWrapper {
    private Settings settings;

    private File fromFile;
    private File toFile;
    private String fromFormat;
    private String toFormat;
    private String extraOptions;

    public PandocWrapper() {
        settings = new Settings();
        settings.setPandocExec("pandoc");
        extraOptions = "";
    }

    public PandocWrapper fromFile(File from, String format) {
        this.fromFile = from;
        this.fromFormat = format;
        return this;
    }

    public PandocWrapper fromFile(File from, InputFormat format) {
        return fromFile(from, format.getFormatName());
    }

    public PandocWrapper toFile(File to, String format) {
        this.toFile = to;
        this.toFormat = format;
        return this;
    }

    public PandocWrapper toFile(File to, OutputFormat format) {
        return toFile(to, format.getFormatName());
    }

    public PandocWrapper addOption(String option) {
        this.extraOptions += " " + option;
        return this;
    }

    public ProcessBuilder build() {
        return new ProcessBuilder(String.format("%s %s --from=%s --to=%s %s --output=%s",
                settings.getPandocExec(), fromFile, fromFormat,
                toFormat, extraOptions, toFile));
    }
}
