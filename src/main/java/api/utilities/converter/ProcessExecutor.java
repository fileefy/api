package api.utilities.converter;

import api.configs.Config;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * This class is used to executed several commands in one process
 */
public class ProcessExecutor {

    private List<List<String>> commands;

    public ProcessExecutor(){
        commands = new ArrayList<>();
    }

    public void addCommand(ProcessBuilder pb){
        commands.add(pb.command());
    }

    public Process execute() throws IOException, InterruptedException {

        List<String> joinedCommands = new ArrayList<>();

        for (var c:commands){
            joinedCommands.add(String.join(" ", c));
        }

        System.out.println("EXECUTION STARTED\n");

        for (var c:joinedCommands){
            System.out.println("Executing: " + c);

            Process p = Runtime.getRuntime().exec(c, null, null);
            p.waitFor();

            if (Config.IS_DEBUG) {
                BufferedReader stdInput = new BufferedReader(new
                        InputStreamReader(p.getErrorStream()));
                BufferedReader stdInput2 = new BufferedReader(new
                        InputStreamReader(p.getInputStream()));

                System.out.println("ERRORS / WARNINGS:\n");
                String s;
                while ((s = stdInput.readLine()) != null) {
                    System.out.println(s);
                }

                System.out.println("OUTPUT: \n");
                while ((s = stdInput2.readLine()) != null) {
                    System.out.println(s);
                }
                System.out.println("Finish\n");
            }
        }

        return null;
    }
}
