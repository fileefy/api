package api.utilities.converter;

import api.configs.Config;
import api.configs.ConverterConfig;
import api.model.Document;
import api.utilities.FileFormat;
import api.utilities.Utilities;
import club.caliope.udc.InputFormat;
import club.caliope.udc.OutputFormat;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import org.apache.commons.io.Charsets;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Converter {

    public static Document convert(List<String> links, FileFormat format, boolean save) throws IOException, InterruptedException {
        // Config new file properties
        String name = Config.RANDOM_STRING.nextString();
        String fileName = name + format.getFormat();
        String filePath = ConverterConfig.SCRIPT_FOLDER + "/out/" + fileName;

        // Create object which will execute commands
        ProcessExecutor pe = new ProcessExecutor();

        // Add command to convert document with python
        ProcessBuilder pb = new ProcessBuilder(Config.getenv("PYTHON"), ConverterConfig.SCRIPT_PATH,
                name, ConverterConfig.SCRIPT_FOLDER);
        pb.command().addAll(links);
        pe.addCommand(pb);

        // Add command to convert document into markdown
        PandocWrapper pw = new PandocWrapper()
                .fromFile(new File(filePath), InputFormat.DOCX)
                .toFile(new File(ConverterConfig.MARKDOWN_PATH), OutputFormat.MARKDOWN)
                .addOption("-s")
                .addOption("--wrap=preserve")
                .addOption("--pdf-engine=xelatex");

        pe.addCommand(pw.build());

        // Execute commands
        pe.execute();

        // Get markdown from the generated file
        String markdown = Files.readString(Paths.get(ConverterConfig.MARKDOWN_PATH), Charsets.UTF_8);

        if (save) {

            // Send document to the Dropbox
            try (InputStream in = new FileInputStream(filePath)) {
                DbxRequestConfig config = DbxRequestConfig.newBuilder("fileefy/0.1-beta").build();
                DbxClientV2 client = new DbxClientV2(config, ConverterConfig.ACCESS_TOKEN);
                client.files().uploadBuilder("/" + fileName).uploadAndFinish(in);
            } catch (Exception e) {
                throw new IOException(e.getMessage());
            }
        }

        // Delete generated files from the file system
        Utilities.cleanUp(new File(filePath).toPath());
        Utilities.cleanUp(new File(ConverterConfig.MARKDOWN_PATH).toPath());

        // Create and return new Document object
        return new Document(links, "/" + fileName, format, markdown);
    }

    public static byte[] convertTo(FileFormat format, String name, String markdown) throws IOException, InterruptedException {
        String filePath = ConverterConfig.MARKDOWN_NAME;
        String newFilePath = "docx" + format.getFormat();
        //String newFilePathSafe = "\"" + newFilePath + "\"";
        ProcessExecutor pe = new ProcessExecutor();

        try {
            File myObj = new File(ConverterConfig.MARKDOWN_NAME);
            if (myObj.createNewFile()) {
                System.out.println("File created: " + myObj.getName());
            } else {
                System.out.println("File already exists.");
            }
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        try (OutputStreamWriter writer =
                     new OutputStreamWriter(new FileOutputStream(ConverterConfig.MARKDOWN_NAME),
                             StandardCharsets.UTF_8)
        ) {
            writer.write(markdown);
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        PandocWrapper pw = new PandocWrapper()
                .fromFile(new File(filePath), InputFormat.MARKDOWN)
                .toFile(new File(newFilePath), Utilities.convertFormat(format))
                .addOption("-s")
                .addOption("--wrap=preserve")
                .addOption("--pdf-engine=xelatex");
        pe.addCommand(pw.build());
        pe.execute();
        byte[] input = null;
        try (FileInputStream fis = new FileInputStream(newFilePath)) {
            input = fis.readAllBytes();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        // Delete generated files from the file system
        Utilities.cleanUp(new File(filePath).toPath());
        Utilities.cleanUp(new File(newFilePath).toPath());

        return input;
    }
}
