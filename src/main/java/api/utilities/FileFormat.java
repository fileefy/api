package api.utilities;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public enum FileFormat {
    DOCX(".docx"),
    EPUB(".epub"),
    FB2(".fb2"),
    MARKDOWN(".md");

    private String format;

    FileFormat(String format) {
        this.format = format;
    }

    public String getFormat() {
        return format;
    }

    private static final Map<String,FileFormat> ENUM_MAP;

    static {
        Map<String,FileFormat> map = new HashMap<String, FileFormat>();
        for (FileFormat instance : FileFormat.values()) {
            map.put(instance.getFormat(),instance);
        }
        ENUM_MAP = Collections.unmodifiableMap(map);
    }

    public static FileFormat get (String name) {
        return ENUM_MAP.get(name);
    }
}