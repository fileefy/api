package api.utilities;

import api.configs.ConverterConfig;
import api.database.HibernateManager;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * The Statistics class is used to generate statistic report.
 */
public class Statistics {

    /**
     * Generates and prepares statistics which can be later send to the client.
     *
     * @return Returns a JSON as a string with statistics values.
     */
    public static String generateStatistics() {

        // Get user amount
        int userCount = HibernateManager.getUserDao().getAll().size();

        // Get document amount
        int documentCount = HibernateManager.getHistoryDao().getAll().size();

        // Connect to the Dropbox
        DbxRequestConfig config = DbxRequestConfig.newBuilder("fileefy/0.1-beta").build();
        DbxClientV2 client = new DbxClientV2(config, ConverterConfig.ACCESS_TOKEN);

        // Get space usage in the Dropbox
        long usedSpace;
        long allSpace;
        try {
            usedSpace = client.users().getSpaceUsage().getUsed();
            allSpace = client.users().getSpaceUsage().getAllocation().getIndividualValue().getAllocated();
        } catch (Exception e) {
            usedSpace = -1;
            allSpace = -1;
        }

        return generateJson(userCount, documentCount, usedSpace, allSpace);
    }

    /**
     * Generates statistics' JSON based on provided values.
     *
     * @param userCount      Amount of users in the system.
     * @param documentCount  Amount of converted documents in the system.
     * @param usedSpace      How much space is used in the Dropbox.
     * @param allocatedSpace How much space is available in the Dropbox.
     * @return
     */
    private static String generateJson(long userCount, long documentCount, long usedSpace,
                                       long allocatedSpace) {
        Gson g = new Gson();
        JsonObject json = new JsonObject();

        // Add values to the JSON
        json.addProperty("userCount", userCount);
        json.addProperty("documentCount", documentCount);
        json.addProperty("usedSpace", usedSpace);
        json.addProperty("allocatedSpace", allocatedSpace);
        json.addProperty("serverTime", LocalDateTime.now().toLocalTime()
                .format(DateTimeFormatter.ofPattern("HH:mm:ss"))
        );

        // Return JSON as a string
        return g.toJson(json);
    }
}
