package api.filters;

import api.utilities.Utilities;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.commons.io.IOUtils;
import org.glassfish.grizzly.utils.Charsets;

import javax.ws.rs.container.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

@JsonContains
@Provider
public class JsonFilter implements ContainerRequestFilter {

    @Context
    private ResourceInfo resourceInfo;

    @Override
    public void filter(ContainerRequestContext request) {
        var callerName = resourceInfo.getResourceMethod().getName();
        String jsonBody = "";

        if (isJson(request)) {
            try {
                jsonBody = IOUtils.toString(request.getEntityStream(), Charsets.UTF8_CHARSET);

                //Return back the read body
                InputStream in = IOUtils.toInputStream(jsonBody);
                request.setEntityStream(in);
            } catch (IOException ex) {
                request.abortWith(Response.status(Response.Status.BAD_REQUEST)
                        .entity(Utilities.generateError(ex.getMessage())).build());
                return;
            }
        } else {
            request.abortWith(Response.status(Response.Status.BAD_REQUEST)
                    .entity(Utilities.generateError("Request body is not a JSON.")).build());
            return;
        }

        JsonObject json = null;

        try {
            json = JsonParser.parseString(jsonBody).getAsJsonObject();
        } catch (Exception e) {
            request.abortWith(Response.status(Response.Status.BAD_REQUEST)
                    .entity(Utilities.generateError("Request body is not a JSON.")).build());
            return;
        }

        for (Method method: resourceInfo.getResourceClass().getMethods()) {
            JsonContains jsonFields = method.getAnnotation(JsonContains.class);

            if (jsonFields != null && callerName.equals(method.getName())) {
                var jsonErrors = checkJsonForFields(jsonFields.value(), json);

                if (jsonErrors != null) {
                    request.abortWith(Response.status(Response.Status.BAD_REQUEST)
                            .entity(Utilities.generateError(jsonErrors)).build());
                }
            }
        }
    }

    public static boolean isJson(ContainerRequestContext request) {
        if (!(request.getMediaType() == null)) {
            // define rules when to read body
            return request.getMediaType().toString().contains("application/json");
        } else {
            return false;
        }
    }

    public static Map<String, String> checkJsonForFields(String[] fields, JsonObject json){
        Map<String, String> errors = new HashMap<String, String>();
        for (String field:fields) {
            if (!json.has(field)) {
                String fieldName = "";

                for (String word:Utilities.splitCamelCase(field)){
                    if (fieldName.equals("")){
                        word = word.substring(0,1).toUpperCase() + word.substring(1);
                    } else {
                        word = word.toLowerCase();
                    }
                    fieldName = fieldName + " " + word;
                }

                errors.put(field, fieldName + " is not provided");
            }
        }

        if (errors.size() == 0){
            return null;
        } else {
            return errors;
        }
    }
}