package api.mailer;

import api.configs.EmailConfig;
import org.simplejavamail.api.email.Email;
import org.simplejavamail.api.mailer.Mailer;
import org.simplejavamail.api.mailer.config.TransportStrategy;
import org.simplejavamail.mailer.MailerBuilder;

/**
 * The EmailSender class is used to send emails with pre-configured mailer.
 */
public class EmailSender {
    private static Mailer mailer;

    private EmailSender() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Creates a new mailer, based on properties in the {@link EmailConfig} class.
     *
     * @implNote Configure method should be called once, during the start of the server.
     */
    public static void configure() {
        mailer = MailerBuilder
                .withSMTPServer(
                        EmailConfig.HOST,
                        EmailConfig.PORT,
                        EmailConfig.USERNAME,
                        EmailConfig.PASSWORD
                )
                .withTransportStrategy(TransportStrategy.SMTP_TLS)
                .buildMailer();
    }

    /**
     * Sends an email.
     *
     * @param email {@link Email} object.
     * @throws InterruptedException Exception is thrown is mailing is disabled in the
     *                              {@link EmailConfig} class
     */
    public static void send(Email email) throws InterruptedException {
        if (EmailConfig.ENABLED) {
            mailer.sendMail(email);
        } else {
            throw new InterruptedException();
        }
    }
}
