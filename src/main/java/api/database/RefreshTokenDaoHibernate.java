package api.database;

import api.model.RefreshToken;
import api.model.User;
import com.google.gson.JsonObject;
import org.hibernate.query.Query;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class RefreshTokenDaoHibernate extends DaoHibernate<RefreshToken> {

    public RefreshToken get(String refreshToken) {
        return HibernateManager.execute(session -> {
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<RefreshToken> cr = cb.createQuery(RefreshToken.class);
            Root<RefreshToken> root = cr.from(RefreshToken.class);
            cr.select(root).where(cb.equal(root.get("token"), refreshToken));
            Query<RefreshToken> query = session.createQuery(cr);
            query.setMaxResults(1);
            List<RefreshToken> result = query.getResultList();

            if (result.size() == 0) {
                return null;
            } else {
                return result.get(0);
            }
        });
    }

    @Override
    public RefreshToken create(JsonObject json) {
        return HibernateManager.execute(session -> new RefreshToken());
    }

    public RefreshToken create(User user, String refreshToken) {
        return HibernateManager.execute(session -> {
            RefreshToken newRefreshToken = new RefreshToken(user, refreshToken);
            session.save(newRefreshToken);
            return newRefreshToken;
        });
    }

    public RefreshToken addRotatedToken(long id, String rotatedToken) {
        return HibernateManager.execute(session -> {
            RefreshToken rt = HibernateManager.getRefreshTokenDao().get(id);

            if (rt.getRotatedTokens() == null) {
                rt.setRotatedTokens(new ArrayList<>());
            }

            rt.getRotatedTokens().add(rotatedToken);
            session.update(rt);
            return rt;
        });
    }
}
