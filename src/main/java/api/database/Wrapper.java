package api.database;

import org.hibernate.Session;

import java.io.IOException;

public interface Wrapper<T> {
    T call(Session session) throws Exception;
}
