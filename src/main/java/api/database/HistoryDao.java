package api.database;

import api.model.Document;
import api.model.HistoryEntry;
import api.model.User;
import api.utilities.FileFormat;

import java.util.List;

public interface HistoryDao extends Dao<HistoryEntry> {
    HistoryEntry getUserSpecific(long userId, long entryId);
    List<HistoryEntry> getAllByUser(long userId);
    void addDocument(HistoryEntry historyEntry, User user, Document document);
    void updateDocument(HistoryEntry historyEntry, String content);
    List<HistoryEntry> getFavourite(long userId);
    String getMarkdown(long userId, long entryId);
    byte[] download(long entryId, FileFormat format);
}
