package api.database;

import api.model.Kindle;
import api.model.User;
import api.security.PasswordEncoder;
import api.utilities.Utilities;
import com.google.gson.JsonObject;
import org.hibernate.query.Query;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.time.LocalDateTime;
import java.util.List;

public class UserDaoHibernate extends DaoHibernate<User> implements UserDao {
    public static final String EMAIL = "email";

    @Override
    public User findByEmail(String email) {
        return HibernateManager.execute(session -> {
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<User> cr = cb.createQuery(User.class);
            Root<User> root = cr.from(User.class);
            cr.select(root).where(cb.equal(root.get(EMAIL), email));
            Query<User> query = session.createQuery(cr);
            query.setMaxResults(1);
            List<User> result = query.getResultList();

            if (result.size() == 0) {
                return null;
            } else {
                return result.get(0);
            }
        });
    }

    @Override
    public User create(JsonObject json) {
        return HibernateManager.execute(session -> {
            boolean isFree = HibernateManager
                    .getUserDao()
                    .checkEmailUsage(json.get(UserDaoHibernate.EMAIL).getAsString());

            if (!isFree) {
                return null;
            } else {
                User user;
                String encryptedPassword = PasswordEncoder
                        .hashPassword(Utilities.decode(json.get("password").getAsString()));

                user = new User(json.get(UserDaoHibernate.EMAIL).getAsString(),
                        encryptedPassword,
                        Utilities.decode(json.get("firstName").getAsString()),
                        Utilities.decode(json.get("lastName").getAsString()));

                session.save(user);
                return user;
            }
        });
    }

    @Override
    public boolean checkEmailUsage(String email) {
        return HibernateManager.execute(session -> {
            var u = HibernateManager.getUserDao().findByEmail(email);
            return u == null;
        });
    }

    @Override
    public void updateKindle(long userId, Kindle kindle) {
        HibernateManager.execute(session -> {
            var u = HibernateManager.getUserDao().get(userId);
            u.setKindle(kindle);
            u.setUpdatedAt(LocalDateTime.now());
            session.update(u);
            return true;
        });
    }
}