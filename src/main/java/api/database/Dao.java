package api.database;

import api.model.Model;
import com.google.gson.JsonObject;

import java.util.List;

public interface Dao<T extends Model> {
    T get(long id);
    List<T> getAll();
    T create(JsonObject json);
    void delete(long id);
    T update(long id, JsonObject json);
    T update(T obj);
}