package api.database;

import api.model.Kindle;
import api.model.User;

public interface UserDao extends Dao<User> {
    User findByEmail(String email);
    boolean checkEmailUsage(String email);
    void updateKindle(long userId, Kindle kindle);
}
