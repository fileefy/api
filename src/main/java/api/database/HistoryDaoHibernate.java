package api.database;

import api.model.Document;
import api.model.HistoryEntry;
import api.model.User;
import api.utilities.FileFormat;
import api.utilities.converter.Converter;
import com.google.gson.JsonObject;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

public class HistoryDaoHibernate extends DaoHibernate<HistoryEntry> implements HistoryDao {

    @Override
    public List<HistoryEntry> getAllByUser(long userId) {
        return HibernateManager.execute(session -> this.getAll()
                .stream()
                .filter(he -> he.user().getId() == userId)
                .collect(Collectors.toList()));
    }

    @Override
    public HistoryEntry create(JsonObject json) {
        return HibernateManager.execute(session -> new HistoryEntry());
    }

    @Override
    public void addDocument(HistoryEntry historyEntry, User user, Document document) {
        HibernateManager.execute(session -> {
            historyEntry.setDocument(document);
            historyEntry.setUser(user);
            session.save(historyEntry);
            return null;
        });
    }

    @Override
    public HistoryEntry getUserSpecific(long userId, long entryId){
        return HibernateManager.execute(session -> {
            List<HistoryEntry> history = HibernateManager.getHistoryDao().getAllByUser(userId);
            var entries = history.stream()
                    .filter(he -> he.getId() == entryId)
                    .collect(Collectors.toList());
            try {
                return entries.get(0);
            } catch (Exception e) {
                return null;
            }
        });
    }

    @Override
    public void updateDocument(HistoryEntry historyEntry, String content) {
        HibernateManager.execute(session -> {
            if (historyEntry.getUpdatedDocument() == null){
                Document d = new Document();
                d.setMarkdownText(content);
                d.setCreatedAt(LocalDateTime.now());
                historyEntry.setUpdatedDocument(d);
            } else {
                historyEntry.getUpdatedDocument().setMarkdownText(content);
            }

            historyEntry.setUpdatedAt(LocalDateTime.now());
            session.update(historyEntry);
            return null;
        });
    }

    @Override
    public List<HistoryEntry> getFavourite(long userId){
        return HibernateManager.execute(session -> {
            var list = this.getAllByUser(userId);
            return list.stream().filter(HistoryEntry::isFavourite).collect(Collectors.toList());
        });
    }

    @Override
    public String getMarkdown(long userId, long entryId){
        return HibernateManager.execute(session -> {
            List<HistoryEntry> history = HibernateManager.getHistoryDao().getAllByUser(userId);
            var entries = history
                                            .stream()
                                            .filter(he -> he.getId() == entryId)
                                            .collect(Collectors.toList());

            if (entries.size() == 0){
                return null;
            } else {
                var entry = entries.get(0);
                Document d;

                if (entry.getUpdatedDocument() != null){
                    d = entry.getUpdatedDocument();
                } else {
                    d = entry.getDocument();
                }

                return d.getMarkdownText();
            }
        });
    }

    @Override
    public byte[] download(long entryId, FileFormat format) {
        return HibernateManager.execute(session -> {
            HistoryEntry historyEntry = this.get(entryId);

            if (historyEntry.getUpdatedDocument() != null){
                return Converter.convertTo(format, historyEntry.getTitle(),
                        historyEntry.getUpdatedDocument().getMarkdownText());
            } else {
                return Converter.convertTo(format, historyEntry.getTitle(),
                        historyEntry.getDocument().getMarkdownText());
            }
        });
    }
}
