package api.database;

import api.model.Kindle;
import api.utilities.KindleModel;
import com.google.gson.JsonObject;

/**
 * The KindleDaoHibernate provides methods to interact with <strong>"kindle"</strong> table
 * in the database.
 */
public class KindleDaoHibernate extends DaoHibernate<Kindle> {

    /**
     * Creates new object in the "kindle" database.
     *
     * @param json JSON with fields required to construct a new object.
     * @return Returns a new {@link Kindle} object.
     */
    @Override
    public Kindle create(JsonObject json) {
        return HibernateManager.execute(session -> {
            Kindle kindle = new Kindle(json.get("kindleEmail").getAsString(),
                    KindleModel.get(json.get("model").getAsString()));
            session.save(kindle);
            return kindle;
        });
    }
}
