package api.database;

import api.configs.Config;
import api.model.Kindle;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public final class HibernateManager {
    private static SessionFactory factory;
    private static UserDao userDao;
    private static DocumentDao documentDao;
    private static HistoryDao historyDao;
    private static Dao<Kindle> kindleDao;
    private static RefreshTokenDaoHibernate refreshTokenDao;

    public static SessionFactory getFactory() {
        return factory;
    }
    public static UserDao getUserDao() {
        return userDao;
    }
    public static DocumentDao getDocumentDao() {
        return documentDao;
    }
    public static HistoryDao getHistoryDao() {
        return historyDao;
    }
    public static Dao<Kindle> getKindleDao() {
        return kindleDao;
    }
    public static RefreshTokenDaoHibernate getRefreshTokenDao() {
        return refreshTokenDao;
    }

    private HibernateManager(){
        throw new IllegalStateException("Utility class");
    }

    public static void set(Configuration cfg){
        try {
            factory = cfg.buildSessionFactory();
        } catch (Throwable ex) {
            System.err.println("Failed to create sessionFactory object." + ex);
            throw new ExceptionInInitializerError(ex);
        }

        setDAOs();
    }

    private static void setDAOs(){
        userDao = new UserDaoHibernate();
        documentDao = new DocumentDaoHibernate();
        historyDao = new HistoryDaoHibernate();
        kindleDao = new KindleDaoHibernate();
        refreshTokenDao = new RefreshTokenDaoHibernate();
    }

    public static void clearFactory(){
        factory = null;
    }

    public static <T> T execute(Wrapper<T> f){
        Session session = factory.openSession();
        Transaction tx = null;
        T returnItem;

        try {
            tx = session.beginTransaction();
            returnItem = f.call(session);
            tx.commit();
        } catch (Exception e) {
            if (tx!=null) {
                tx.rollback();
            }
            if (Boolean.parseBoolean(Config.getenv("IS_DEBUG"))) {
                e.printStackTrace();
            }
            throw new RuntimeException(e.getMessage());
        } finally {
            session.close();
        }
        return returnItem;
    }
}
