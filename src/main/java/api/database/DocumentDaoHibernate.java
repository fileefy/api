package api.database;

import api.configs.ConverterConfig;
import api.model.Document;
import api.utilities.FileFormat;
import api.utilities.converter.Converter;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class DocumentDaoHibernate extends DaoHibernate<Document> implements DocumentDao {

    @Override
    public Document create(JsonObject json) {
        return HibernateManager.execute(session -> {
            Gson gson = new Gson();
            List<String> links = gson.fromJson(json.get("links").getAsJsonArray(),
                    new TypeToken<ArrayList<String>>(){}.getType());
            Document document = Converter.convert(links, FileFormat.get(json.get("format").getAsString()), true);
            session.save(document);
            return document;
        });
    }

    @Override
    public InputStream getDocument(long id) {
        return HibernateManager.execute(session -> {
            Document d = HibernateManager.getDocumentDao().get(id);

            if (d == null) {
                throw new Exception("No document with this id");
            }

            DbxRequestConfig config = DbxRequestConfig.newBuilder("fileefy/0.1-beta").build();
            DbxClientV2 client = new DbxClientV2(config, ConverterConfig.ACCESS_TOKEN);
            var fileData = client.files().download(d.getFileUrl());
            return fileData.getInputStream();
        });
    }
}
