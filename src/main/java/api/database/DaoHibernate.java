package api.database;

import api.model.Model;
import api.model.TimestampModel;
import api.utilities.Utilities;
import com.google.gson.JsonObject;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.lang.reflect.ParameterizedType;
import java.time.LocalDateTime;
import java.util.List;

public abstract class DaoHibernate<T extends Model> implements Dao<T> {
    private final Class<T> type;

    public DaoHibernate(){
        this.type = (Class<T>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];
    }

    @Override
    public T get(long id) {
        return (T) HibernateManager.execute(session -> session.get(type, id));
    }

    @Override
    public List<T> getAll() {
        return HibernateManager.execute(session -> {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<T> criteria = builder.createQuery(type);
            criteria.from(type);
            return session.createQuery(criteria).getResultList();
        });
    }

    @Override
    public void delete(long id) {
        HibernateManager.execute(session -> {
            T user = session.get(type, id);
            session.delete(user);
            return null;
        });
    }

    @Override
    public T update(long id, JsonObject json) {
        return HibernateManager.execute(session -> {
            T obj = session.get(type, id);
            Utilities.updateEntity(obj, json);

            if (obj instanceof TimestampModel) {
                ((TimestampModel) obj).setUpdatedAt(LocalDateTime.now());
            }

            session.update(obj);
            return obj;
        });
    }

    public T update(T obj) {
        return HibernateManager.execute(session -> {
            if (obj instanceof TimestampModel) {
                ((TimestampModel) obj).setUpdatedAt(LocalDateTime.now());
            }

            session.update(obj);
            return obj;
        });
    }
}
