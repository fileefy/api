package api.database;

import api.model.Document;

import java.io.InputStream;

public interface DocumentDao extends Dao<Document> {
    InputStream getDocument(long id);
}
