package api.model;

import javax.persistence.*;

/**
 * The Model class is the <strong>base</strong> class for all models.
 */
@MappedSuperclass
public abstract class Model {

    /**
     * Represents unique identifier of the entity in the table.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    protected long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
