package api.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.time.LocalDateTime;

/**
 * The TimestampModel class is used to represent entities which keep track
 * of creation time and last update.
 */
@MappedSuperclass
public abstract class TimestampModel extends Model {

    /**
     * Time when the object was created and added to the database.
     */
    @Column(name = "created_at")
    private LocalDateTime createdAt;

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * Time when the object was last time updated.
     */
    @Column(name = "updated_at")
    private LocalDateTime updatedAt;

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * Basic constructor to initialize object fields.
     *
     * @implNote By default {@link TimestampModel#createdAt} is set to current time.<br>
     * And {@link TimestampModel#updatedAt} is set to {@link null}.
     */
    public TimestampModel() {
        this.createdAt = LocalDateTime.now();
        this.updatedAt = null;
    }
}
