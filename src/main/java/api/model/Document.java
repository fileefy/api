package api.model;

import api.utilities.FileFormat;
import com.vladmihalcea.hibernate.type.array.ListArrayType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * The Document class represents document entity from the database.
 * Used to store information about one specific document.
 */
@Entity
@Table(name = "document")
@TypeDef(
        name = "list-array",
        typeClass = ListArrayType.class
)
public class Document extends Model {

    //region Properties
    /**
     * List of the URLs from which the document was generated.
     */
    @Type(type = "list-array")
    @Column(name = "urls")
    private List<String> urls;

    public List<String> getUrls() {
        return urls;
    }

    public void setUrls(List<String> urls) {
        this.urls = urls;
    }

    /**
     * URL of the document on the Dropbox.
     */
    @Column(name = "file_url")
    private String fileUrl;

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    /**
     * Document's format. One of the {@link FileFormat} values.
     *
     * @implNote In the database format is stored as a String value. <br>
     * To get a {@link FileFormat} value use {@link Document#format()}. <br>
     * To set from the {@link FileFormat} value use {@link Document#setFormat(FileFormat)}.
     */
    @Column(name = "format")
    private String format;

    public String getFormat() {
        return format;
    }

    private void setFormat(String format) {
        this.format = format;
    }

    public void setFormat(FileFormat format) {
        this.format = format.getFormat();
    }

    public FileFormat format() {
        return FileFormat.valueOf(format);
    }

    @Column(name = "cover_image")
    private String coverUrl;

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    /**
     * Creation date of the document
     */
    @Column(name = "created_at")
    private LocalDateTime createdAt;

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * Markdown representation of the article. This representation can be used
     * to convert article into different formats using the Pandoc.
     */
    @Column(name = "md_representation")
    private String markdownText;

    public String getMarkdownText() {
        return markdownText;
    }

    public void setMarkdownText(String markdownText) {
        this.markdownText = markdownText;
    }
    //endregion

    /**
     * Base constructor.
     */
    public Document() {
        this.createdAt = LocalDateTime.now();
    }

    /**
     * Constructs HistoryEntry object from the provided parameters.
     *
     * @param urls         List of urls from which this document was created.
     * @param fileUrl      URL to the file on the Dropbox.
     * @param format       Format of the converted document.
     * @param markdownText Markdown representation of the document.
     */
    public Document(List<String> urls, String fileUrl, FileFormat format, String markdownText) {
        this.urls = urls;
        this.fileUrl = fileUrl;
        this.markdownText = markdownText;
        this.createdAt = LocalDateTime.now();

        setFormat(format);
    }
}
