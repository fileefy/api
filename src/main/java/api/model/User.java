package api.model;

import api.security.Role;

import javax.persistence.*;
import java.util.List;

/**
 * The User class represents user entity from the database.
 */
@Entity
@Table(name = "users")
public class User extends TimestampModel {

    //region Properties
    /**
     * First name of the user.
     */
    @Column(name = "first_name")
    private String firstName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Last name of the user.
     */
    @Column(name = "last_name")
    private String lastName;

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Email of the user.
     */
    @Column(name = "email")
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String newEmail) {
        this.email = newEmail;
    }

    /**
     * Password of the user.
     *
     * @implSpec Password must not be exposed through endpoints.
     * @implNote By default password is hashed using BCrypt. <br>
     * {@link api.security.PasswordEncoder#hashPassword(String)}
     */
    @Column(name = "password")
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "avatar_url")
    private String avatarUrl;

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    /**
     * User's e-book specification.
     */
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "kindle_id", referencedColumnName = "id")
    private Kindle kindle;

    public Kindle getKindle() {
        return kindle;
    }

    public void setKindle(Kindle kindleId) {
        this.kindle = kindleId;
    }

    /**
     * User's current role.
     *
     * @implNote <ul>
     * <li>In the database role is stored as an integer (index of the {@link Role} enum).</li>
     * <li>One of the roles available in the {@link Role} enum.</li>
     * </ul>
     */
    @Column(name = "role")
    private Integer role;

    public Integer getRole() {
        return role;
    }

    private void setRole(Integer role) {
        this.role = role;
    }

    /**
     * Method to get {@link Role} value from the user object.
     *
     * @return One of the {@link Role} enum's values.
     */
    public Role role() {
        return Role.values()[role];
    }

    /**
     * Sets {@link User#role} from the {@link Role} enum.
     *
     * @param role New role of the user.
     */
    public void assignRole(Role role) {
        this.role = role.ordinal();
    }

    /**
     * User's history of converted articles.
     *
     * @see HistoryEntry
     */
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user", cascade = CascadeType.ALL)
    private List<HistoryEntry> history;

    public List<HistoryEntry> history() {
        return history;
    }

    /**
     * User's history of converted articles.
     *
     * @see HistoryEntry
     */
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user", cascade = CascadeType.ALL)
    private List<RefreshToken> refreshToken;

    public List<RefreshToken> refreshToken() {
        return refreshToken;
    }
    //endregion

    /**
     * Base constructor.
     *
     * @implNote Forwards logic to the super class.
     * @see TimestampModel
     */
    public User() {
        super();
    }

    /**
     * Constructs User object from the provided parameters.
     *
     * @param email     User's email.
     * @param password  User's password (preferably hashed with
     *                  {@link api.security.PasswordEncoder#hashPassword(String)}).
     * @param firstName User's first name.
     * @param lastName  User's last name.
     * @implNote By default user's {@link User#role} is set to {@link Role#USER}.
     * To create user with other roles use {@link User#User(String, String, String, String, Role)}.
     */
    public User(String email, String password, String firstName, String lastName) {
        this();

        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;

        assignRole(Role.USER);
    }

    /**
     * Constructs User object with specific role
     *
     * @param email     User's email.
     * @param password  User's password (preferably hashed with
     *                  {@link api.security.PasswordEncoder#hashPassword(String)}).
     * @param firstName User's first name.
     * @param lastName  User's last name.
     * @param role      User's role.
     */
    public User(String email, String password, String firstName, String lastName, Role role) {
        this(email, password, firstName, lastName);
        assignRole(role);
    }
}
