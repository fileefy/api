package api.model;

import javax.persistence.*;

/**
 * The HistoryEntry class represents history entry entity from the database.
 * Is used to show user's history of the converted articles.
 */
@Entity
@Table(name = "history_entry")
public class HistoryEntry extends TimestampModel {

    //region Properties
    /**
     * User to whom this history entry is assigned.
     */
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    public User user() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Title of the entry.
     */
    @Column(name = "title")
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Short summary of the history entry.
     */
    @Column(name = "description")
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Color of the entry (is needed only for the clients).
     */
    @Column(name = "color")
    private String color;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    /**
     * Shows if user has marked history entry as a favourit one.
     */
    @Column(name = "is_favourite")
    private boolean isFavourite;

    public boolean isFavourite() {
        return isFavourite;
    }

    public void setFavourite(boolean favourite) {
        isFavourite = favourite;
    }

    /**
     * Initial version of the converted document assigned to this
     * history entry.
     */
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "document_id", referencedColumnName = "id")
    private Document document;

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

    /**
     * Updated version of the document.
     */
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "updated_document_id", referencedColumnName = "id")
    private Document updatedDocument;

    public Document getUpdatedDocument() {
        return updatedDocument;
    }

    public void setUpdatedDocument(Document updatedDocument) {
        this.updatedDocument = updatedDocument;
    }
    //endregion

    /**
     * Base constructor.
     *
     * @implNote Forwards logic to the super class.
     * @see TimestampModel
     */
    public HistoryEntry() {
        super();
    }

    /**
     * Constructs HistoryEntry object from the provided parameters.
     *
     * @param user     User to whom this entry will be assigned.
     * @param document Initial version of the converted document.
     */
    public HistoryEntry(User user, Document document) {
        this();
        this.user = user;
        this.document = document;
    }
}
