package api.model;

import api.utilities.KindleModel;

import javax.persistence.*;

/**
 * The Kindle class represents information about user's Kindle ebook.
 */
@Entity
@Table(name = "kindle")
public class Kindle extends Model {

    //region Properties
    /**
     * Kindle email, where documents will be send.
     */
    @Column(name = "kindle_email")
    private String kindleEmail;

    public String getKindleEmail() {
        return kindleEmail;
    }

    public void setKindleEmail(String kindleEmail) {
        this.kindleEmail = kindleEmail;
    }

    /**
     * Model of the Kindle.
     */
    @Column(name = "model")
    private String model;

    public String getModel() {
        return model;
    }

    public KindleModel model() {
        return KindleModel.valueOf(model);
    }

    private void setModel(String model) {
        this.model = model;
    }

    public void setModel(KindleModel model) {
        this.model = model.getModel();
    }
    //endregion

    /**
     * Base constructor.
     */
    public Kindle() {
        this.kindleEmail = null;
        this.model = null;
    }

    /**
     * Constructs Kindle object from the provided parameters.
     *
     * @param kindleEmail Valid Kindle email.
     */
    public Kindle(String kindleEmail, KindleModel model) {
        this.kindleEmail = kindleEmail;
        setModel(model);
    }
}
