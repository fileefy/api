package api.model;

import com.vladmihalcea.hibernate.type.array.ListArrayType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * The RefreshToken class represents information about currently active
 * refresh token.
 */
@Entity
@Table(name = "refresh_tokens")
@TypeDef(
        name = "list-array",
        typeClass = ListArrayType.class
)
public class RefreshToken extends Model {

    //region Properties
    /**
     * User to whom this refresh token was issued.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName="id", nullable = false)
    private User user;

    public User user() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Valid refresh token.
     */
    @Column(name = "refresh_token")
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String refreshToken) {
        this.token = refreshToken;
    }

    /**
     * Rotated refresh tokens. They are still valid, however, must
     * not be used. If any of these tokens are used, the whole token family
     * must be removed.
     */
    @Type(type = "list-array")
    @Column(name = "rotated_tokens")
    private List<String> rotatedTokens;

    public List<String> getRotatedTokens() {
        return rotatedTokens;
    }

    public void setRotatedTokens(List<String> tokenFamily) {
        this.rotatedTokens = tokenFamily;
    }

    /**
     * Base constructor.
     */
    public RefreshToken() {
    }

    /**
     * Constructs RefreshToken object from the provided parameters.
     *
     * @param user         User to whom refresh token is issued.
     * @param token Valid refresh token.
     */
    public RefreshToken(User user, String token) {
        this.user = user;
        this.token = token;
        this.rotatedTokens = new ArrayList<>();
    }
}
