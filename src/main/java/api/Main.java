package api;

import api.configs.Config;
import api.configs.CustomApplicationConfig;
import api.resources.StatisticsResource;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.websockets.WebSocketAddOn;
import org.glassfish.grizzly.websockets.WebSocketEngine;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;

import javax.ws.rs.core.UriBuilder;
import java.io.IOException;
import java.net.URI;

class Main {

    private static URI getBaseURI(int port) {
        return UriBuilder.fromUri("http://0.0.0.0/").port(port).build();
    }

    protected static void startServer(URI uri) throws IOException {
        System.out.println("Starting grizzly...");
        CustomApplicationConfig customApplicationConfig = new CustomApplicationConfig();
        HttpServer server = GrizzlyHttpServerFactory.createHttpServer(uri,
                customApplicationConfig, false);

        WebSocketAddOn webSocketAddOn = new WebSocketAddOn();
        server.getListeners().forEach(listener -> {
            listener.registerAddOn(webSocketAddOn);
        });

        // register my websocket app
        StatisticsResource webSocketApp = new StatisticsResource();
        WebSocketEngine.getEngine().register("", "/admin/statistics", webSocketApp);

        server.start();
    }

    public static void main(String[] args) throws IOException {
        Config.configure();
        String port = Config.getenv("PORT");
        URI uri = getBaseURI(Integer.parseInt(port));

        startServer(uri);
    }
}
