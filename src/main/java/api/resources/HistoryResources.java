package api.resources;

import api.database.HibernateManager;
import api.filters.JsonContains;
import api.model.Document;
import api.model.HistoryEntry;
import api.security.Role;
import api.security.Secured;
import api.utilities.FileFormat;
import api.utilities.Utilities;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

@Path("/history")
public class HistoryResources {
    @Context
    private UriInfo uriInfo;

    @GET
    @Path("/all")
    @Secured(Role.ADMIN)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllHistoryEntries() {
        List<HistoryEntry> history = HibernateManager.getHistoryDao().getAll();
        GenericEntity<List<HistoryEntry>> entity = new GenericEntity<>(history) {
        };
        return Response.ok(entity).build();
    }

    @GET
    @Secured
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll(@Context SecurityContext securityContext) {
        long userId = Long.parseLong(securityContext.getUserPrincipal().getName());
        List<HistoryEntry> history = HibernateManager.getHistoryDao().getAllByUser(userId);
        history.forEach(he -> he.setDocument(null));
        history.forEach(he -> he.setUpdatedDocument(null));
        GenericEntity<List<HistoryEntry>> entity = new GenericEntity<>(history) {
        };
        return Response.ok(entity).build();
    }

    @GET
    @Path("{id}")
    @Secured
    @Produces(MediaType.APPLICATION_JSON)
    public Response getHistoryEntry(@PathParam("id") int entryId,
                                    @Context SecurityContext securityContext) {
        long userId = Long.parseLong(securityContext.getUserPrincipal().getName());
        var entry = HibernateManager.getHistoryDao().getUserSpecific(userId, entryId);

        if (entry == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            entry.setDocument(null);
            entry.setUpdatedDocument(null);
            return Response.ok(entry).build();
        }
    }

    @GET
    @Path("{id}/content")
    @Secured
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMarkdownText(@PathParam("id") int entryId,
                                    @Context SecurityContext securityContext) {
        long userId = Long.parseLong(securityContext.getUserPrincipal().getName());
        String markdown = HibernateManager.getHistoryDao().getMarkdown(userId, entryId);

        if (markdown == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            JsonObject j = new JsonObject();
            j.addProperty("content", markdown);
            Gson g = new Gson();

            // Return the token on the response
            return Response.ok(g.toJson(j)).build();
        }
    }

    @PATCH
    @Secured
    @Path("{id}/content")
    @JsonContains({"newContent"})
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateDocument(String jsonBody,
                                   @PathParam("id") int entryId,
                                   @Context SecurityContext securityContext) throws UnsupportedEncodingException {
        long userId = Long.parseLong(securityContext.getUserPrincipal().getName());

        JsonObject json = JsonParser.parseString(jsonBody).getAsJsonObject();

        var entry = HibernateManager.getHistoryDao().getUserSpecific(userId, entryId);

        if (entry == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            HibernateManager.getHistoryDao().updateDocument(entry,
                    Utilities.decode(json.get("newContent").getAsString()));
            return Response.ok().build();
        }
    }

    @POST
    @Path("{id}/download")
    @Secured
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @JsonContains({"format"})
    public Response download(@PathParam("id") int entryId,
                             String jsonBody,
                             @Context SecurityContext securityContext) throws IOException {
        long userId = Long.parseLong(securityContext.getUserPrincipal().getName());
        JsonObject json = JsonParser.parseString(jsonBody).getAsJsonObject();

        try {
            for (var historyEntry : HibernateManager.getHistoryDao().getAllByUser(userId)) {
                if (historyEntry.getId() == entryId) {
                    return Response
                            .ok(HibernateManager.getHistoryDao()
                                            .download(entryId, FileFormat.valueOf(json.get("format").getAsString())),
                                    MediaType.APPLICATION_OCTET_STREAM)
                            .header("Content-Disposition", "attachment; filename=\""
                                    + Utilities.encode(historyEntry.getTitle())
                                    + FileFormat.valueOf(json.get("format").getAsString()).getFormat()
                                    + "\"")
                            .build();
                }
            }
        } catch (Exception e) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(Utilities.generateError("Please provide a supported format."))
                    .build();
        }

        return Response
                .status(Response.Status.BAD_REQUEST)
                .entity(Utilities.generateError("Please provide a valid document id."))
                .build();
    }

    @GET
    @Path("favourite")
    @Secured
    @Produces(MediaType.APPLICATION_JSON)
    public Response getFavourite(@Context SecurityContext securityContext) {
        long userId = Long.parseLong(securityContext.getUserPrincipal().getName());
        List<HistoryEntry> history = HibernateManager.getHistoryDao().getFavourite(userId);
        history.forEach(he -> he.setDocument(null));
        history.forEach(he -> he.setUpdatedDocument(null));
        GenericEntity<List<HistoryEntry>> entity = new GenericEntity<>(history) {
        };
        return Response.ok(entity).build();
    }

    @POST
    @Path("add")
    @Secured
    @JsonContains({"title", "description", "links", "format", "color"})
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response create(String jsonBody,
                           @Context SecurityContext securityContext) {
        long userId = Long.parseLong(securityContext.getUserPrincipal().getName());

        JsonObject json = JsonParser.parseString(jsonBody).getAsJsonObject();
        Document d;
        HistoryEntry historyEntry;

        try {
            d = HibernateManager.getDocumentDao().create(json);
            historyEntry = HibernateManager.getHistoryDao().create(null);
            historyEntry.setTitle(Utilities.decode(json.get("title").getAsString()));
            historyEntry.setDescription(Utilities.decode(json.get("description").getAsString()));
            historyEntry.setColor(json.get("color").getAsString());
            HibernateManager.getHistoryDao().addDocument(historyEntry, HibernateManager.getUserDao().get(userId), d);
        } catch (Exception exception) {
            return Response.status(Response.Status.BAD_REQUEST).entity(exception).build();
        }

        // return document id
        JsonObject j = new JsonObject();
        j.addProperty("documentId", d.getId());
        j.addProperty("historyEntryId", historyEntry.getId());
        Gson g = new Gson();

        StatisticsResource.broadcastStatistics();

        return Response.ok(g.toJson(j)).build();
    }

    @PATCH
    @Path("{id}")
    @Secured
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateHistoryEntry(String jsonBody,
                                       @PathParam("id") int entryId,
                                       @Context SecurityContext securityContext) {
        long userId = Long.parseLong(securityContext.getUserPrincipal().getName());

        JsonObject json = JsonParser.parseString(jsonBody).getAsJsonObject();
        var entry = HibernateManager.getHistoryDao().getUserSpecific(userId, entryId);

        if (entry != null) {
            try {
                HibernateManager.getHistoryDao().update(entryId, json);
            } catch (Exception exception) {
                return Response.status(Response.Status.BAD_REQUEST).build();
            }
            return Response.ok().build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @DELETE
    @Secured
    @Path("{id}")
    public Response deleteHistoryEntry(@PathParam("id") int entryId,
                                       @Context SecurityContext securityContext) {
        long userId = Long.parseLong(securityContext.getUserPrincipal().getName());

        for (var historyEntry : HibernateManager.getHistoryDao().getAllByUser(userId)) {
            if (historyEntry.getId() == entryId) {
                HibernateManager.getHistoryDao().delete(entryId);
                StatisticsResource.broadcastStatistics();
                return Response.ok().build();
            }
        }

        return Response.noContent().build();
    }
}
