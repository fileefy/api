package api.resources;

import api.utilities.Statistics;
import org.glassfish.grizzly.websockets.DataFrame;
import org.glassfish.grizzly.websockets.WebSocket;
import org.glassfish.grizzly.websockets.WebSocketApplication;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * The StatisticsResource class is used to provide WebSocket interface for statistics.
 */
public class StatisticsResource extends WebSocketApplication {

    /**
     * This websocket.
     */
    private WebSocket socket;

    private static final Object Mutex = new Object();
    /**
     * List of all the websocket connections.
     */
    private static final Set<StatisticsResource> connections
            = new CopyOnWriteArraySet<>();

    /**
     * Method executed after connection with the client was established.
     */
    @Override
    public void onConnect(WebSocket socket) {
        super.onConnect(socket);
        connections.add(this);
        this.socket = socket;
        this.socket.send(Statistics.generateStatistics());
    }

    /**
     * Method executed after connection with the client was closed or lost.
     */
    @Override
    public void onClose(WebSocket socket, DataFrame frame) {
        super.onClose(socket, frame);
        connections.remove(this);
    }

    /**
     * Broadcasts statistics to all existing websocket connections.
     */
    public static void broadcastStatistics() {
        connections.forEach(endpoint -> {
            synchronized (Mutex) {
                endpoint.socket.send(Statistics.generateStatistics());
            }
        });
    }
}
