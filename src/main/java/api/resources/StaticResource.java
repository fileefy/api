package api.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.io.File;

@Path("/")
public class StaticResource {
    @Context
    private UriInfo uriInfo;

    @GET
    public Response sayHello() {
        File file = new File("public/index.html");
        return Response.ok(file).build();
    }
}
