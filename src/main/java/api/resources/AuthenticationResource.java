package api.resources;

import api.configs.Config;
import api.database.HibernateManager;
import api.filters.JsonContains;
import api.model.User;
import api.security.PasswordEncoder;
import api.security.Secured;
import api.security.TokenManager;
import api.security.Tokens;
import api.utilities.Utilities;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import javax.ws.rs.*;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import java.io.UnsupportedEncodingException;

import static javax.ws.rs.core.Cookie.DEFAULT_VERSION;
import static javax.ws.rs.core.NewCookie.DEFAULT_MAX_AGE;

/**
 * The AuthenticationResource class contains all the endpoints used to authenticate
 * users.
 */
@Path("/authentication")
public class AuthenticationResource {

    /**
     * Name of the cookie in which refresh token is stored on the client side.
     */
    private static final String REFRESH_COOKIE = "refreshToken";

    /**
     * Endpoint to verify token's validity.
     */
    @GET
    @Secured
    public Response verify() {
        return Response.ok().build();
    }

    /**
     * Authenticates user based on provided credentials.
     *
     * @param credentials JSON body with user's credentials.
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @JsonContains({"email", "password", "isRememberMe"})
    public Response authenticateUser(String credentials) throws UnsupportedEncodingException {

        // Get credentials
        JsonObject json = JsonParser.parseString(credentials).getAsJsonObject();
        String email = json.get("email").getAsString();
        String password = Utilities.decode(json.get("password").getAsString());

        try {

            // Authenticate the user using the credentials provided
            User u = authenticate(email, password);

            // Issue a token for the user
            Tokens tokens = TokenManager.issueTokens(u.getId(), json.get("isRememberMe").getAsBoolean());

            return createResponse(tokens);
        } catch (Exception e) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }

    /**
     * Endpoint to refresh user's token.
     *
     * @param cookie Cookie with the refresh token.
     */
    @POST
    @Path("refresh")
    public Response refresh(@CookieParam(REFRESH_COOKIE) Cookie cookie) {

        try {

            // Issue new token
            Tokens newTokens = TokenManager.issueTokens(cookie.getValue());

            // Create response body
            return createResponse(newTokens);
        } catch (Exception e) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }

    /**
     * Endpoint to logout users from the system.
     *
     * @param cookie Cookie with the refresh token.
     */
    @DELETE
    @Secured
    public Response logout(@CookieParam(REFRESH_COOKIE) Cookie cookie) {

        try {
            boolean status = TokenManager.invalidate(cookie.getValue());
            // Delete the token
            if (status) {
                return Response.ok().build();
            } else {
                return Response.status(Response.Status.UNAUTHORIZED).build();
            }
        } catch (Exception e) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }

    /**
     * Creates response with two tokens.
     *
     * @param tokens Object which contains access and refresh tokens.
     * @return Returns Response which should be sent to the client.
     */
    private Response createResponse(Tokens tokens) {

        // Create response body
        JsonObject j = new JsonObject();
        j.addProperty("token", tokens.accessToken);
        Gson g = new Gson();

        // Return a new token in the body and a new refresh token in HttpOnly cookie
        return Response
                .ok(g.toJson(j))
                .cookie(new NewCookie(REFRESH_COOKIE, tokens.refreshToken, "/authentication",
                        "", DEFAULT_VERSION, null, DEFAULT_MAX_AGE,
                        null, !Config.ON_LOCAL, true))
                .build();
    }

    /**
     * Verifies provided credentials.
     *
     * @param email    User's email.
     * @param password User's password.
     * @return Returns {@link User} object, related to the provided credentials.
     * @throws Exception Is thrown if password is incorrect.
     */
    private User authenticate(String email, String password) throws Exception {

        // Find user
        User u = HibernateManager.getUserDao().findByEmail(email);
        if (u == null) {
            throw new Exception();
        }

        // Check user's password
        if (!PasswordEncoder.checkPassword(password, u.getPassword())) {
            throw new Exception();
        }

        return u;
    }
}