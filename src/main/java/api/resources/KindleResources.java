package api.resources;

import api.configs.EmailConfig;
import api.database.HibernateManager;
import api.filters.JsonContains;
import api.mailer.EmailSender;
import api.model.HistoryEntry;
import api.model.Kindle;
import api.model.User;
import api.security.Secured;
import api.utilities.FileFormat;
import api.utilities.Utilities;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.simplejavamail.api.email.Email;
import org.simplejavamail.email.EmailBuilder;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 * The KindleResources class contains all the endpoints used to interact
 * with user's Kindle e-book.
 */
@Path("/kindle")
public class KindleResources {

    /**
     * Endpoint to get information about user's Kindle.
     *
     * @param securityContext Dependency injection with user's information.
     */
    @GET
    @Secured
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@Context SecurityContext securityContext) {

        // Get user
        long userId = Long.parseLong(securityContext.getUserPrincipal().getName());
        User user = HibernateManager.getUserDao().get(userId);

        return Response.ok(user.getKindle()).build();
    }

    /**
     * Endpoint to add user's kindle information.
     *
     * @param jsonBody        JSON body with data.
     * @param securityContext Dependency injection with user's information.
     */
    @POST
    @Secured
    @JsonContains({"model", "kindleEmail"})
    @Consumes(MediaType.APPLICATION_JSON)
    public Response set(String jsonBody,
                        @Context SecurityContext securityContext) {

        // Get user
        long userId = Long.parseLong(securityContext.getUserPrincipal().getName());

        // Get body
        JsonObject json = JsonParser.parseString(jsonBody).getAsJsonObject();

        Response r = validateEmail(json);
        if (r != null) {
            return r;
        }

        // Update kindle information
        try {
            User user = HibernateManager.getUserDao().get(userId);
            if (user.getKindle() == null) {
                Kindle k = HibernateManager.getKindleDao().create(json);
                HibernateManager.getUserDao().updateKindle(userId, k);
            } else {
                return Response.status(Response.Status.BAD_REQUEST)
                        .entity(Utilities.generateError("User already has an assigned Kindle. Consider updating it."))
                        .build();
            }
        } catch (Exception exception) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        return Response.ok().build();
    }

    /**
     * Endpoint to update kindle information.
     *
     * @param jsonBody        JSON body with data.
     * @param securityContext Dependency injection with user's information.
     */
    @PATCH
    @Secured
    @Consumes(MediaType.APPLICATION_JSON)
    public Response update(String jsonBody,
                           @Context SecurityContext securityContext) {

        // Get user
        long userId = Long.parseLong(securityContext.getUserPrincipal().getName());
        User user = HibernateManager.getUserDao().get(userId);

        // Get body
        JsonObject json = JsonParser.parseString(jsonBody).getAsJsonObject();

        Response r = validateEmail(json);
        if (r != null) {
            return r;
        }

        if (user.getKindle() == null) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(Utilities.generateError("User does not an assigned Kindle yet."))
                    .build();
        }
        HibernateManager.getKindleDao().update(user.getKindle().getId(), json);


        return Response.ok().build();
    }

    /**
     * Endpoint to send documents to the Kindle.
     *
     * @param securityContext Dependency injection with user's information.
     * @param historyEntryId  Id of the history entry with document, which should be set to
     *                        the Kindle.
     */
    @POST
    @Path("send/{historyEntryId}")
    @Secured
    public Response send(@Context SecurityContext securityContext,
                         @PathParam("historyEntryId") long historyEntryId) {

        // Get user
        long userId = Long.parseLong(securityContext.getUserPrincipal().getName());
        User user = HibernateManager.getUserDao().get(userId);

        // Get history entry
        HistoryEntry historyEntry = HibernateManager.getHistoryDao().getUserSpecific(userId, historyEntryId);
        if (historyEntry == null) {
            return Response.status(404).build();
        }

        // Get kindle email
        String kindleEmail;
        try {
            kindleEmail = user.getKindle().getKindleEmail();
        } catch (Exception e) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(Utilities.generateError("Kindle is not set."))
                    .build();
        }

        // Generate email
        Email email = EmailBuilder.startingBlank()
                .from(EmailConfig.SENDER_NAME, EmailConfig.SENDER_ADDRESS)
                .to(kindleEmail)
                .withSubject("convert")
                .withAttachment(historyEntry.getTitle() + ".docx",
                        HibernateManager.getHistoryDao().download(historyEntryId, FileFormat.DOCX),
                        "application/vnd.openxmlformats-officedocument.wordprocessingml.document") // .docx
                .buildEmail();

        // Send email
        try {
            EmailSender.send(email);
        } catch (Exception ie) {
            return Response
                    .status(Response.Status.ACCEPTED)
                    .entity(Utilities.generateError("Emails are temporally turned off."))
                    .build();
        }

        return Response.ok().build();
    }

    private Response validateEmail(JsonObject json) {
        if (json.has("kindleEmail")
                && !Utilities.checkEmail(json.get("kindleEmail").getAsString(), true)) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(Utilities.generateError("Not Kindle email."))
                    .build();
        } else {
            return null;
        }
    }
}
