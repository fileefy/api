package api.resources;

import api.database.HibernateManager;
import api.filters.JsonContains;
import api.model.User;
import api.security.Role;
import api.security.Secured;
import api.utilities.Utilities;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.List;
import java.util.stream.Collectors;

@Path("user")
public class UserResource {

    private final static String EMAIL_FIELD = "email";

    @Context
    private UriInfo uriInfo;

    //Get all user
    @GET
    @Secured({Role.ADMIN})
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUsers() {
        List<User> users = HibernateManager.getUserDao().getAll();
        users.forEach((user) -> user.setPassword(null));
        GenericEntity<List<User>> entity = new GenericEntity<>(users) {
        };
        return Response.ok(entity).build();
    }

    //Get one user by his id
    @GET
    @Secured
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUser(@Context SecurityContext securityContext) {
        long userId = Long.parseLong(securityContext.getUserPrincipal().getName());

        User user = HibernateManager.getUserDao().get(userId);
        if (user == null) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(Utilities.generateError("User does not exist."))
                    .build();
        } else {
            user.setPassword(null);
            return Response.ok(user).build();
        }
    }

    @GET
    @Secured({Role.ADMIN})
    @Path("/pending")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPendingUsers() {
        List<User> users = HibernateManager
                .getUserDao()
                .getAll()
                .stream()
                .filter(x -> x.role() == Role.USER_PENDING).collect(Collectors.toList());

        Gson g = new Gson();
        JsonArray j = new JsonArray();

        for (User user : users) {
            JsonObject json = new JsonObject();

            // Add values to the JSON
            json.addProperty("id", user.getId());
            json.addProperty(EMAIL_FIELD, user.getEmail());

            j.add(json);
        }

        return Response.ok(g.toJson(j)).build();
    }

    @POST
    @Secured({Role.USER})
    @Path("/request")
    @Produces(MediaType.APPLICATION_JSON)
    public Response requestUpgrade(@Context SecurityContext securityContext) {
        long userId = Long.parseLong(securityContext.getUserPrincipal().getName());
        User u = HibernateManager.getUserDao().get(userId);

        u.assignRole(Role.USER_PENDING);
        HibernateManager.getUserDao().update(u);

        Gson g = new Gson();
        JsonObject json = new JsonObject();

        // Add values to the JSON
        json.addProperty("role", u.getRole());

        return Response.ok(g.toJson(json)).build();
    }

    @POST
    @Secured({Role.ADMIN})
    @Path("/pending/approve/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response approveUser(@PathParam("id") long userId) {
        User u = HibernateManager.getUserDao().get(userId);
        if (u != null) {
            if (u.role() == Role.USER_PENDING) {
                u.assignRole(Role.UPGRADED_USER);
                HibernateManager.getUserDao().update(u);

                return Response.ok().build();
            } else {
                return Response.status(Response.Status.BAD_REQUEST)
                        .entity(Utilities.generateError("Unable to approve."))
                        .build();
            }
        } else {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(Utilities.generateError("User does not exist."))
                    .build();
        }
    }

    //Create new user based on his credentials
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @JsonContains({EMAIL_FIELD, "password", "firstName", "lastName"})
    public Response createUser(String jsonBody) {
        JsonObject json = JsonParser.parseString(jsonBody).getAsJsonObject();

        Response r = validateEmail(json);
        if (r != null) {
            return r;
        }

        User u = (HibernateManager.getUserDao().create(json));

        if (u == null) {
            return Response.status(Response.Status.CONFLICT)
                    .entity(Utilities.generateError("Email is already in use")).build();
        } else {
            u.setPassword(null);
            StatisticsResource.broadcastStatistics();
            return Response.ok(u).build();
        }
    }

    //Update user information
    @PATCH
    @Secured
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateUser(String jsonBody,
                               @Context SecurityContext securityContext) {
        long userId = Long.parseLong(securityContext.getUserPrincipal().getName());

        JsonObject json = JsonParser.parseString(jsonBody).getAsJsonObject();

        Response r = validateEmail(json);
        if (r != null) {
            return r;
        }

        try {
            HibernateManager.getUserDao().update(userId, json);
        } catch (Exception exception) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        return Response.ok().build();
    }

    @DELETE
    @Secured
    public Response deleteUser(@Context SecurityContext securityContext) {
        long userId = Long.parseLong(securityContext.getUserPrincipal().getName());

        if (HibernateManager.getUserDao().get(userId) == null) {
            return Response.status(204).build();
        }
        HibernateManager.getUserDao().delete(userId);
        StatisticsResource.broadcastStatistics();
        return Response.ok().build();
    }

    private Response validateEmail(JsonObject json) {
        if (json.has(EMAIL_FIELD)) {
            if (!Utilities.checkEmail(json.get(EMAIL_FIELD).getAsString(), false)) {
                return Response.status(Response.Status.BAD_REQUEST)
                        .entity(Utilities.generateError("Bad email."))
                        .build();
            } else if (!HibernateManager.getUserDao().checkEmailUsage(json.get(EMAIL_FIELD).getAsString())) {
                return Response.status(Response.Status.CONFLICT)
                        .entity(Utilities.generateError("Email is already in use."))
                        .build();
            }
        }
        return null;
    }
}
