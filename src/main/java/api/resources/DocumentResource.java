package api.resources;

import api.database.HibernateManager;
import api.filters.JsonContains;
import api.model.Document;
import api.security.Role;
import api.security.Secured;
import api.utilities.FileFormat;
import api.utilities.converter.Converter;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Path("/documents")
public class DocumentResource {
    @Context
    private UriInfo uriInfo;

    @GET
    @Secured(Role.ADMIN)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllDocuments() {
        var documents = HibernateManager.getDocumentDao().getAll();
        GenericEntity<List<Document>> docs = new GenericEntity<>(documents) {
        };
        return Response.ok().entity(docs).build();
    }

    @GET
    @Path("/formats")
    @Secured
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllFormats() {
        var formats = FileFormat.values();

        GenericEntity<FileFormat[]> docs = new GenericEntity<>(formats) {
        };
        return Response.ok().entity(docs).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @JsonContains({"link"})
    public Response convertDocument(String jsonBody) {
        JsonObject json = JsonParser.parseString(jsonBody).getAsJsonObject();
        List<String> s = new ArrayList<>(Collections.singletonList(json.get("link").getAsString()));
        String markdown;
        try {
            markdown = Converter.convert(s, FileFormat.DOCX, false).getMarkdownText();
        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        JsonObject j = new JsonObject();
        j.addProperty("content", markdown);
        Gson g = new Gson();

        // Return the token on the response
        return Response.ok(g.toJson(j)).build();
    }
}
