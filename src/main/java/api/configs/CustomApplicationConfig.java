package api.configs;

import api.filters.CORSFilter;
import api.filters.JsonFilter;
import api.security.AuthenticationFilter;
import api.security.AuthorizationFilter;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.server.ResourceConfig;

import java.util.logging.Level;
import java.util.logging.Logger;

public class    CustomApplicationConfig extends ResourceConfig
{
    public CustomApplicationConfig()
    {
        packages("api.resources"); // find all resource endpoint classes in this package
        // log exchanged http messages
        register(new LoggingFeature(Logger.getLogger(LoggingFeature.DEFAULT_LOGGER_NAME),
                Level.ALL, LoggingFeature.Verbosity.PAYLOAD_ANY, LoggingFeature.DEFAULT_MAX_ENTITY_SIZE));
        register(new CORSFilter());
        register(new JsonFilter());
        register(new AuthenticationFilter());
        register(new AuthorizationFilter());
    }
}
