package api.configs;

public class ConverterConfig {
    public static final String SCRIPT_PATH = "fileefy-converter/main.py";
    public static final String SCRIPT_FOLDER = "fileefy-converter";
    public static final String MARKDOWN_PATH = ConverterConfig.SCRIPT_FOLDER + "/out/markdown.md";
    public static final String MARKDOWN_NAME = "markdown.md";

    public static String ACCESS_TOKEN;
}
