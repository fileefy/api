package api.configs;

import api.model.*;
import org.hibernate.cfg.Configuration;

public class HibernateConfig {

    private HibernateConfig() {
        throw new IllegalStateException("Utility class");
    }

    public static Configuration local() {
        return addClasses(new Configuration()
                .configure());
    }

    public static Configuration production() {
        return addClasses(new Configuration()
                .setProperty("hibernate.connection.url", "jdbc:postgresql://"
                        + Config.getenv("DB_HOST") + ":"
                        + Config.getenv("DB_PORT") + "/"
                        + Config.getenv("DB_NAME") + "?"
                        + "verifyServerCertificate=false" + "&"
                        + "useSSL=true" + "&"
                        + "sslmode=require")
                .setProperty("hibernate.connection.username", Config.getenv("DB_USERNAME"))
                .setProperty("hibernate.connection.password", Config.getenv("DB_PASSWORD"))
                .setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQL10Dialect")
                .setProperty("hibernate.connection.driver_class", "org.postgresql.Driver")
                .setProperty("hibernate.temp.use_jdbc_metadata_defaults", "false")
                .setProperty("hibernate.hbm2ddl.auto", "none")
                .setProperty("hibernate.show_sql", "true")
        );
    }

    public static Configuration test() {
        return addClasses(new Configuration()
                .setProperty("hibernate.connection.url", "jdbc:postgresql://"
                        + Config.getenv("DB_HOST_TEST") + ":"
                        + Config.getenv("DB_PORT_TEST") + "/"
                        + Config.getenv("DB_NAME_TEST") + "?"
                        + "verifyServerCertificate=false" + "&"
                        + "useSSL=true" + "&"
                        + "sslmode=require")
                .setProperty("hibernate.connection.username", Config.getenv("DB_USERNAME_TEST"))
                .setProperty("hibernate.connection.password", Config.getenv("DB_PASSWORD_TEST"))
                .setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQL10Dialect")
                .setProperty("hibernate.connection.driver_class", "org.postgresql.Driver")
                .setProperty("hibernate.temp.use_jdbc_metadata_defaults", "false")
                .setProperty("hibernate.hbm2ddl.auto", "none")
                .setProperty("hibernate.show_sql", "true")
        );
    }

    private static Configuration addClasses(Configuration cfg) {
        return cfg.addAnnotatedClass(User.class)
                .addAnnotatedClass(Kindle.class)
                .addAnnotatedClass(Document.class)
                .addAnnotatedClass(HistoryEntry.class)
                .addAnnotatedClass(RefreshToken.class);
    }
}
