package api.configs;

import api.database.HibernateManager;
import api.security.TokenManager;
import api.utilities.RandomString;
import io.github.cdimascio.dotenv.Dotenv;
import io.github.cdimascio.dotenv.DotenvException;

import java.security.SecureRandom;
import java.util.Random;

public class Config {
    public static boolean ON_LOCAL = System.getenv("DATABASE_URL") == null;
    public static boolean IS_DEBUG;
    public static boolean IS_TESTING;
    public static final Random RANDOM = new SecureRandom();
    public static final RandomString RANDOM_STRING = new RandomString(15, Config.RANDOM);
    private static Dotenv DOTENV;

    public static String getenv(String name){
        if (Config.ON_LOCAL){
            return Config.DOTENV.get(name);
        } else {
            return System.getenv(name);
        }
    }

    public static void configure() {
        if (Config.ON_LOCAL) {
            if (System.getenv("IS_TESTING") != null){
                IS_TESTING = true;
            }

            try {
                Config.DOTENV = Dotenv.load();
            } catch (Exception e) {
                throw new DotenvException("No .env file");
            }
        }

        if (IS_TESTING) {
            HibernateManager.set(HibernateConfig.test());
        } else if (!ON_LOCAL) {
            HibernateManager.set(HibernateConfig.production());
        } else {
            HibernateManager.set(HibernateConfig.local());
        }


        Config.IS_DEBUG = Boolean.parseBoolean(Config.getenv("IS_DEBUG"));
        ConverterConfig.ACCESS_TOKEN = Config.getenv("DROPBOX_TOKEN");
        TokenManager.configure();
        EmailConfig.configure();
    }
}
