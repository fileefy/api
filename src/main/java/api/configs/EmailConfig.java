package api.configs;

import api.mailer.EmailSender;

/**
 * The EmailConfig class stores data about email sender.
 */
public class EmailConfig {
    public static String HOST;
    public static int PORT;
    public static String USERNAME;
    public static String PASSWORD;

    public static String SENDER_ADDRESS;
    public static String SENDER_NAME;
    public static boolean ENABLED;

    private EmailConfig() {
        throw new IllegalStateException("Utility class");
    }

    public static void configure() {
        HOST = Config.getenv("MAIL_HOST");
        PORT = Integer.parseInt(Config.getenv("MAIL_PORT"));
        USERNAME = Config.getenv("MAIL_USERNAME");
        PASSWORD = Config.getenv("MAIL_PASSWORD");

        SENDER_ADDRESS = Config.getenv("MAIL_SENDER_ADDRESS");
        SENDER_NAME = Config.getenv("MAIL_SENDER_NAME");
        ENABLED = Boolean.parseBoolean(Config.getenv("MAIL_ENABLED"));

        EmailSender.configure();
    }
}
