package api.filters;

import com.google.gson.JsonObject;
import org.junit.Assert;
import org.junit.Test;

public class JsonFilterTest {

    @Test
    public void jsonFieldsTest() {

        String value = "val";
        String[] v = {"test"};

        // create json object
        JsonObject j = new JsonObject();
        j.addProperty("test", value);

        Assert.assertNull(JsonFilter.checkJsonForFields(v, j));
    }

    @Test
    public void jsonNullFieldsTest() {

        String value = "val";
        String[] v = {"test"};

        // create json object
        JsonObject j = new JsonObject();
        j.addProperty("t", value);

        Assert.assertNotNull(JsonFilter.checkJsonForFields(v, j));
    }
}