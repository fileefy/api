package api.resources;

import api.configs.Config;
import api.configs.CustomApplicationConfig;
import api.database.HibernateManager;
import api.model.User;
import api.security.Role;
import api.security.TokenManager;
import api.utilities.FileFormat;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

public class DocumentResourceTest extends JerseyTest {

    private final String email = "test@email.com";
    private final String password = "pass";
    private final String firstName = "TestName";
    private final String lastName = "TestLastName";
    private String accessToken;
    private User user;

    @Override
    protected Application configure() {
        return new ResourceConfig(new CustomApplicationConfig());
    }

    @BeforeClass
    public static void initMe() {
        Config.IS_TESTING = true;
        Config.configure();
    }

    @Before
    public void setUpChild() {
        HibernateManager.execute((session -> {
            session.createNativeQuery("truncate table history_entry restart identity cascade")
                    .executeUpdate();
            session.createNativeQuery("truncate table document restart identity cascade")
                    .executeUpdate();
            session.createNativeQuery("truncate table refresh_tokens restart identity cascade")
                    .executeUpdate();
            session.createNativeQuery("truncate table users restart identity cascade")
                    .executeUpdate();
            session.createNativeQuery("truncate table kindle restart identity cascade")
                    .executeUpdate();
            return null;
        }));

        // Create a new user
        JsonObject j = new JsonObject();
        j.addProperty("email", email);
        j.addProperty("password", password);
        j.addProperty("firstName", firstName);
        j.addProperty("lastName", lastName);

        this.user = HibernateManager.getUserDao().create(j);
        this.user.assignRole(Role.ADMIN);
        this.user = HibernateManager.getUserDao().update(this.user);

        this.accessToken = TokenManager.issueTokens(user.getId(), false).accessToken;
    }

    @Test
    public void getAllDocumentsTest() {
        String jsonBody = target("documents").request()
                .header("Authorization", "Bearer " + this.accessToken)
                .get(String.class);
        JsonArray json = JsonParser.parseString(jsonBody).getAsJsonArray();
        Assert.assertEquals(0, json.size());
    }

    @Test
    public void getAllFormatsTest() {
       String jsonBody = target("documents/formats").request()
                .header("Authorization", "Bearer " + this.accessToken)
                .get(String.class);
        JsonArray json = JsonParser.parseString(jsonBody).getAsJsonArray();
        Assert.assertEquals(FileFormat.values().length, json.size());
    }

    @Test
    public void convertDocumentTest() {
        Gson gson = new Gson();

        String link = "https://en.wikipedia.org/wiki/Personal_computer";

        JsonObject j = new JsonObject();
        j.addProperty("link", link);

        String jsonBody = target("documents").request()
                .header("Authorization", "Bearer " + this.accessToken)
                .post(Entity.entity(gson.toJson(j), MediaType.APPLICATION_JSON), String.class);

        Assert.assertTrue(JsonParser.parseString(jsonBody).getAsJsonObject().has("content"));
    }
}