package api.resources;

import api.configs.Config;
import api.configs.CustomApplicationConfig;
import api.database.HibernateManager;
import api.model.User;
import api.security.Role;
import api.security.TokenManager;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.glassfish.jersey.client.HttpUrlConnectorProvider;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

public class UserResourceTest extends JerseyTest {

    private final String email = "test@email.com";
    private final String password = "pass";
    private final String firstName = "TestName";
    private final String lastName = "TestLastName";
    private String accessToken;
    private User user;

    @Override
    protected Application configure() {
        return new ResourceConfig(new CustomApplicationConfig());
    }

    @BeforeClass
    public static void initMe() {
        Config.IS_TESTING = true;
        Config.configure();
    }

    @Before
    public void setUpChild() {
        HibernateManager.execute((session -> {
            session.createNativeQuery("truncate table history_entry restart identity cascade")
                    .executeUpdate();
            session.createNativeQuery("truncate table document restart identity cascade")
                    .executeUpdate();
            session.createNativeQuery("truncate table refresh_tokens restart identity cascade")
                    .executeUpdate();
            session.createNativeQuery("truncate table users restart identity cascade")
                    .executeUpdate();
            session.createNativeQuery("truncate table kindle restart identity cascade")
                    .executeUpdate();
            return null;
        }));

        // Create a new user
        JsonObject j = new JsonObject();
        j.addProperty("email", email);
        j.addProperty("password", password);
        j.addProperty("firstName", firstName);
        j.addProperty("lastName", lastName);

        this.user = HibernateManager.getUserDao().create(j);
        this.user.assignRole(Role.ADMIN);
        this.user = HibernateManager.getUserDao().update(this.user);

        this.accessToken = TokenManager.issueTokens(user.getId(), false).accessToken;
    }

    @Test(expected = NotAuthorizedException.class)
    public void getUsersFailTest() {
        final List<User> users = target("user/all").request().get(List.class);
        Assert.assertEquals(1, users.size());
    }

    @Test
    public void getUsersTest() {
        final List<User> users = target("user/all").request()
                .header("Authorization", "Bearer " + this.accessToken)
                .get(List.class);
        Assert.assertEquals(1, users.size());
    }

    @Test
    public void getUserTest() {
        final User user = target("user").request()
                .header("Authorization", "Bearer " + this.accessToken)
                .get(User.class);
        Assert.assertEquals(email, user.getEmail());
    }

    @Test
    public void getPendingUsersTest() {
        final List<User> users = target("user/pending").request()
                .header("Authorization", "Bearer " + this.accessToken)
                .get(List.class);
        Assert.assertEquals(0, users.size());

        JsonObject j = new JsonObject();
        j.addProperty("email", "test" + email);
        j.addProperty("password", password);
        j.addProperty("firstName", firstName);
        j.addProperty("lastName", lastName);
        User u = HibernateManager.getUserDao().create(j);

        u.assignRole(Role.USER_PENDING);
        HibernateManager.getUserDao().update(u);

        final List<User> usersPending = target("user/pending").request()
                .header("Authorization", "Bearer " + this.accessToken)
                .get(List.class);
        Assert.assertEquals(1, usersPending.size());
    }

    @Test
    public void requestUpgradeTest() {
        this.user.assignRole(Role.USER);
        this.user = HibernateManager.getUserDao().update(this.user);

        String jsonBody = target("user/request").request()
                .header("Authorization", "Bearer " + this.accessToken)
                .post(null, String.class);
        JsonObject json = JsonParser.parseString(jsonBody).getAsJsonObject();

        Assert.assertEquals(1, json.get("role").getAsInt());
        Assert.assertEquals(Role.USER_PENDING, HibernateManager.getUserDao().get(this.user.getId()).role());
    }

    @Test
    public void createUserTest() {
        Gson gson = new Gson();

        JsonObject j = new JsonObject();
        j.addProperty("email", "test" + email);
        j.addProperty("password", password);
        j.addProperty("firstName", firstName);
        j.addProperty("lastName", lastName);

        User u = target("user").request()
                .post(Entity.entity(gson.toJson(j), MediaType.APPLICATION_JSON), User.class);
        Assert.assertEquals("test" + email, u.getEmail());
        Assert.assertEquals(Role.USER, HibernateManager.getUserDao().get(u.getId()).role());
    }

    @Test
    public void createUserEmailErrorTest() {
        Gson gson = new Gson();

        JsonObject j = new JsonObject();
        j.addProperty("email", email);
        j.addProperty("password", password);
        j.addProperty("firstName", firstName);
        j.addProperty("lastName", lastName);

        Assert.assertEquals(Response.Status.CONFLICT.getStatusCode(), target("user").request()
                .post(Entity.entity(gson.toJson(j), MediaType.APPLICATION_JSON)).getStatus());
    }

    @Test
    public void approveUserTest() {

        // Create a new user
        JsonObject j = new JsonObject();
        j.addProperty("email", "test" + email);
        j.addProperty("password", password);
        j.addProperty("firstName", firstName);
        j.addProperty("lastName", lastName);

        User u = HibernateManager.getUserDao().create(j);

        // Change user's role
        u.assignRole(Role.USER_PENDING);
        u = HibernateManager.getUserDao().update(u);

        // Execute and assert
        target("user/pending/approve/" + u.getId()).request()
                .header("Authorization", "Bearer " + this.accessToken)
                .post(null, String.class);
        Assert.assertEquals(Role.UPGRADED_USER, HibernateManager.getUserDao().get(u.getId()).role());

        // Assert that same user cannot be approved again
        Assert.assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), target("user/pending/approve/" +
                u.getId()).request()
                .header("Authorization", "Bearer " + this.accessToken)
                .post(null).getStatus());
    }

    @Test
    public void approveUserFailTest() {

        // Assert that an error is thrown for a random user
        Assert.assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), target("user/pending/approve/9999").request()
                .header("Authorization", "Bearer " + this.accessToken)
                .post(null).getStatus());
    }

    @Test
    public void updateUserTest() {
        Gson gson = new Gson();

        JsonObject j = new JsonObject();
        j.addProperty("firstName", "test" + firstName);

        target("user").request()
                .header("Authorization", "Bearer " + this.accessToken)
                .build("PATCH", Entity.entity(gson.toJson(j), MediaType.APPLICATION_JSON))
                .property(HttpUrlConnectorProvider.SET_METHOD_WORKAROUND, true)
                .invoke();
        Assert.assertEquals("test" + firstName, HibernateManager.getUserDao()
                .get(this.user.getId()).getFirstName()
        );
    }

    @Test
    public void deleteUserTest() {
        target("user").request()
                .header("Authorization", "Bearer " + this.accessToken)
                .delete();
        Assert.assertEquals(0, HibernateManager.getUserDao().getAll().size());

        // Check that deletion of the same user is forbidden
        Assert.assertEquals(Response.Status.FORBIDDEN.getStatusCode(), target("user").request()
                .header("Authorization", "Bearer " + this.accessToken)
                .delete().getStatus());
    }
}