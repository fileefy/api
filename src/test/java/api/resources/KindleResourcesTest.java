package api.resources;

import api.configs.Config;
import api.configs.CustomApplicationConfig;
import api.database.HibernateManager;
import api.model.Document;
import api.model.HistoryEntry;
import api.model.Kindle;
import api.model.User;
import api.security.TokenManager;
import api.utilities.KindleModel;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.glassfish.jersey.client.HttpUrlConnectorProvider;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class KindleResourcesTest extends JerseyTest {

    private final String email = "test@email.com";
    private final String password = "pass";
    private final String firstName = "TestName";
    private final String lastName = "TestLastName";
    private String accessToken;
    private User user;

    @Override
    protected Application configure() {
        return new ResourceConfig(new CustomApplicationConfig());
    }

    @BeforeClass
    public static void initMe() {
        Config.IS_TESTING = true;
        Config.configure();
    }

    @Before
    public void setUpChild() {
        HibernateManager.execute((session -> {
            session.createNativeQuery("truncate table history_entry restart identity cascade")
                    .executeUpdate();
            session.createNativeQuery("truncate table document restart identity cascade")
                    .executeUpdate();
            session.createNativeQuery("truncate table refresh_tokens restart identity cascade")
                    .executeUpdate();
            session.createNativeQuery("truncate table users restart identity cascade")
                    .executeUpdate();
            session.createNativeQuery("truncate table kindle restart identity cascade")
                    .executeUpdate();
            return null;
        }));

        // Create a new user
        JsonObject j = new JsonObject();
        j.addProperty("email", email);
        j.addProperty("password", password);
        j.addProperty("firstName", firstName);
        j.addProperty("lastName", lastName);

        this.user = HibernateManager.getUserDao().create(j);

        this.accessToken = TokenManager.issueTokens(user.getId(), false).accessToken;
    }

    @Test
    public void getKindleTest() {
        Assert.assertNull(target("kindle").request()
                .header("Authorization", "Bearer " + this.accessToken)
                .get(Kindle.class));

        this.user.setKindle(new Kindle("test@test.com", KindleModel.OTHER));
        this.user = HibernateManager.getUserDao().update(this.user);

        String jsonBody = target("kindle").request()
                .header("Authorization", "Bearer " + this.accessToken)
                .get(String.class);
        JsonObject json = JsonParser.parseString(jsonBody).getAsJsonObject();

        Assert.assertEquals("test@test.com", json.get("kindleEmail").getAsString());
    }

    @Test
    public void setKindleTest() {
        Gson g = new Gson();
        JsonObject j = new JsonObject();

        j.addProperty("model", "Other");
        j.addProperty("kindleEmail", "test@kindle.com");

        Assert.assertEquals(Response.Status.OK.getStatusCode(), target("kindle").request()
                .header("Authorization", "Bearer " + this.accessToken)
                .post(Entity.entity(g.toJson(j), MediaType.APPLICATION_JSON)).getStatus());

        Assert.assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), target("kindle").request()
                .header("Authorization", "Bearer " + this.accessToken)
                .post(Entity.entity(g.toJson(j), MediaType.APPLICATION_JSON)).getStatus());
    }

    @Test
    public void setKindleFailTest() {
        Gson g = new Gson();
        JsonObject j = new JsonObject();

        j.addProperty("model", "Other");
        j.addProperty("kindleEmail", "test@fail.com");

        Assert.assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), target("kindle").request()
                .header("Authorization", "Bearer " + this.accessToken)
                .post(Entity.entity(g.toJson(j), MediaType.APPLICATION_JSON)).getStatus());
    }

    @Test
    public void updateKindleTest() {
        this.user.setKindle(new Kindle("test@test.com", KindleModel.OTHER));
        this.user = HibernateManager.getUserDao().update(this.user);

        Gson g = new Gson();
        JsonObject j = new JsonObject();
        String newEmail = "secondTest@kindle.com";
        j.addProperty("kindleEmail", newEmail);

        target("kindle").request()
                .header("Authorization", "Bearer " + this.accessToken)
                .build("PATCH", Entity.entity(g.toJson(j), MediaType.APPLICATION_JSON))
                .property(HttpUrlConnectorProvider.SET_METHOD_WORKAROUND, true)
                .invoke();

        Assert.assertEquals(newEmail, HibernateManager.getUserDao().get(user.getId()).getKindle().getKindleEmail());
    }

    @Test
    public void updateKindleFailTest() {
        this.user.setKindle(new Kindle("test@test.com", KindleModel.OTHER));
        this.user = HibernateManager.getUserDao().update(this.user);

        Gson g = new Gson();
        JsonObject j = new JsonObject();
        String newEmail = "secondTest@fail.com";
        j.addProperty("kindleEmail", newEmail);

        Assert.assertEquals(Response.Status.BAD_REQUEST.getStatusCode(),target("kindle").request()
                .header("Authorization", "Bearer " + this.accessToken)
                .build("PATCH", Entity.entity(g.toJson(j), MediaType.APPLICATION_JSON))
                .property(HttpUrlConnectorProvider.SET_METHOD_WORKAROUND, true)
                .invoke().getStatus());
    }

    @Test
    public void updateKindleNoKindleFailTest() {
        Gson g = new Gson();
        JsonObject j = new JsonObject();
        String newEmail = "secondTest@kindle.com";
        j.addProperty("kindleEmail", newEmail);

        Assert.assertEquals(Response.Status.BAD_REQUEST.getStatusCode(),target("kindle").request()
                .header("Authorization", "Bearer " + this.accessToken)
                .build("PATCH", Entity.entity(g.toJson(j), MediaType.APPLICATION_JSON))
                .property(HttpUrlConnectorProvider.SET_METHOD_WORKAROUND, true)
                .invoke().getStatus());
    }

    @Test
    public void sendDocumentToKindleTest() {
        this.user.setKindle(new Kindle("test@kindle.com", KindleModel.OTHER));
        this.user = HibernateManager.getUserDao().update(this.user);

        String link = "https://en.wikipedia.org/wiki/Personal_computer";

        JsonObject j = new JsonObject();
        var links = new JsonArray();
        links.add(link);
        j.add("links", links);
        j.addProperty("format", ".docx");

        Document d = HibernateManager.getDocumentDao().create(j);
        HistoryEntry he = HibernateManager.getHistoryDao().create(null);
        HibernateManager.getHistoryDao().addDocument(he, this.user, d);

        Assert.assertEquals(Response.Status.OK.getStatusCode(), target("kindle/send/" + he.getId()).request()
                .header("Authorization", "Bearer " + this.accessToken)
                .post(null).getStatus());
    }

    @Test
    public void sendDocumentToKindleNoDocumentTest() {
        Assert.assertEquals(Response.Status.NOT_FOUND.getStatusCode(), target("kindle/send/9999").request()
                .header("Authorization", "Bearer " + this.accessToken)
                .post(null).getStatus());
    }

    @Test
    public void sendDocumentToKindleNoKindleTest() {

        String link = "https://en.wikipedia.org/wiki/Personal_computer";

        JsonObject j = new JsonObject();
        var links = new JsonArray();
        links.add(link);
        j.add("links", links);
        j.addProperty("format", ".docx");

        Document d = HibernateManager.getDocumentDao().create(j);
        HistoryEntry he = HibernateManager.getHistoryDao().create(null);
        HibernateManager.getHistoryDao().addDocument(he, this.user, d);

        Assert.assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), target("kindle/send/" + he.getId())
                .request()
                .header("Authorization", "Bearer " + this.accessToken)
                .post(null).getStatus());
    }
}