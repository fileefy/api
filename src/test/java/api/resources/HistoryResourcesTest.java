package api.resources;

import api.configs.Config;
import api.configs.CustomApplicationConfig;
import api.database.HibernateManager;
import api.model.Document;
import api.model.HistoryEntry;
import api.model.User;
import api.security.Role;
import api.security.TokenManager;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.glassfish.jersey.client.HttpUrlConnectorProvider;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

public class HistoryResourcesTest extends JerseyTest {

    private final String email = "test@email.com";
    private final String password = "pass";
    private final String firstName = "TestName";
    private final String lastName = "TestLastName";
    private String accessToken;
    private User user;

    @Override
    protected Application configure() {
        return new ResourceConfig(new CustomApplicationConfig());
    }

    @BeforeClass
    public static void initMe() {
        Config.IS_TESTING = true;
        Config.configure();
    }

    @Before
    public void setUpChild() {
        HibernateManager.execute((session -> {
            session.createNativeQuery("truncate table history_entry restart identity cascade")
                    .executeUpdate();
            session.createNativeQuery("truncate table document restart identity cascade")
                    .executeUpdate();
            session.createNativeQuery("truncate table refresh_tokens restart identity cascade")
                    .executeUpdate();
            session.createNativeQuery("truncate table users restart identity cascade")
                    .executeUpdate();
            session.createNativeQuery("truncate table kindle restart identity cascade")
                    .executeUpdate();
            return null;
        }));

        // Create a new user
        JsonObject j = new JsonObject();
        j.addProperty("email", email);
        j.addProperty("password", password);
        j.addProperty("firstName", firstName);
        j.addProperty("lastName", lastName);

        this.user = HibernateManager.getUserDao().create(j);
        this.user.assignRole(Role.ADMIN);
        this.user = HibernateManager.getUserDao().update(this.user);

        this.accessToken = TokenManager.issueTokens(user.getId(), false).accessToken;
    }

    @Test
    public void getHistoryEntriesOfAllUsersTest() {
        var he = HibernateManager.getHistoryDao().create(null);
        HibernateManager.getHistoryDao().addDocument(he, this.user, null);
        final List<HistoryEntry> historyEntryList = target("history/all").request()
                .header("Authorization", "Bearer " + this.accessToken)
                .get(List.class);
        Assert.assertEquals(1, historyEntryList.size());
    }

    @Test
    public void getHistoryEntriesTest() {
        final List<HistoryEntry> historyEntryList1 = target("history").request()
                .header("Authorization", "Bearer " + this.accessToken)
                .get(List.class);
        Assert.assertEquals(0, historyEntryList1.size());

        var he = HibernateManager.getHistoryDao().create(null);
        HibernateManager.getHistoryDao().addDocument(he, this.user, null);

        final List<HistoryEntry> historyEntryList2 = target("history").request()
                .header("Authorization", "Bearer " + this.accessToken)
                .get(List.class);
        Assert.assertEquals(1, historyEntryList2.size());
    }

    @Test
    public void getHistoryEntryTest() {
        var he1 = HibernateManager.getHistoryDao().create(null);
        he1.setTitle("Test");
        HibernateManager.getHistoryDao().addDocument(he1, this.user, null);

        final HistoryEntry he2 = target("history/" + he1.getId()).request()
                .header("Authorization", "Bearer " + this.accessToken)
                .get(HistoryEntry.class);
        Assert.assertEquals("Test", he2.getTitle());
    }

    @Test
    public void getHistoryEntryFailTest() {
        Assert.assertEquals(Response.Status.NOT_FOUND.getStatusCode(),
                target("history/" + 999).request()
                .header("Authorization", "Bearer " + this.accessToken)
                .get().getStatus());
    }

    @Test
    public void getMarkdownTextTest() {
        String link = "https://en.wikipedia.org/wiki/Personal_computer";

        JsonObject j = new JsonObject();
        var links = new JsonArray();
        links.add(link);
        j.add("links", links);
        j.addProperty("format", ".docx");

        Document d = HibernateManager.getDocumentDao().create(j);
        HistoryEntry he1 = HibernateManager.getHistoryDao().create(null);
        HibernateManager.getHistoryDao().addDocument(he1, this.user, d);

        String jsonBody = target("history/" + he1.getId() + "/content").request()
                .header("Authorization", "Bearer " + this.accessToken)
                .get(String.class);

        Assert.assertTrue(JsonParser.parseString(jsonBody).getAsJsonObject().has("content"));
    }

    @Test
    public void getMarkdownTextFailTest() {
        HistoryEntry he1 = HibernateManager.getHistoryDao().create(null);
        HibernateManager.getHistoryDao().addDocument(he1, this.user, new Document());

        Assert.assertEquals(Response.Status.NOT_FOUND.getStatusCode(),
                target("history/" + he1.getId() + "/content").request()
                        .header("Authorization", "Bearer " + this.accessToken)
                        .get().getStatus());
    }

    @Test
    public void updateDocumentTest() {
        Gson gson = new Gson();

        var he = HibernateManager.getHistoryDao().create(null);
        HibernateManager.getHistoryDao().addDocument(he, this.user, null);

        JsonObject j = new JsonObject();
        j.addProperty("newContent", "Test");

        target("history/" + he.getId() + "/content").request()
                .header("Authorization", "Bearer " + this.accessToken)
                .build("PATCH", Entity.entity(gson.toJson(j), MediaType.APPLICATION_JSON))
                .property(HttpUrlConnectorProvider.SET_METHOD_WORKAROUND, true)
                .invoke();

        Assert.assertEquals("Test", HibernateManager.getHistoryDao().getMarkdown(this.user.getId(), he.getId()));
    }

    @Test
    public void updateDocumentFailTest() {
        Gson gson = new Gson();

        var he = HibernateManager.getHistoryDao().create(null);
        HibernateManager.getHistoryDao().addDocument(he, this.user, null);

        JsonObject j = new JsonObject();
        j.addProperty("newContent", "Test");

        Assert.assertEquals(Response.Status.NOT_FOUND.getStatusCode(), target("history/" + 999 + "/content")
                .request()
                .header("Authorization", "Bearer " + this.accessToken)
                .build("PATCH", Entity.entity(gson.toJson(j), MediaType.APPLICATION_JSON))
                .property(HttpUrlConnectorProvider.SET_METHOD_WORKAROUND, true)
                .invoke().getStatus());
    }

    @Test
    public void downloadTest() {
        Gson gson = new Gson();

        String link = "https://en.wikipedia.org/wiki/Personal_computer";

        JsonObject j = new JsonObject();
        var links = new JsonArray();
        links.add(link);
        j.add("links", links);
        j.addProperty("format", ".docx");

        Document d = HibernateManager.getDocumentDao().create(j);
        HistoryEntry he1 = HibernateManager.getHistoryDao().create(null);
        he1.setTitle("test");
        HibernateManager.getHistoryDao().addDocument(he1, this.user, d);

        j = new JsonObject();
        j.addProperty("format", "DOCX");

        var r= target("history/" + he1.getId() + "/download")
                .request()
                .header("Authorization", "Bearer " + this.accessToken)
                .post(Entity.entity(gson.toJson(j), MediaType.APPLICATION_JSON));
        Assert.assertEquals(Response.Status.OK.getStatusCode(), r.getStatus());
        Assert.assertEquals(MediaType.APPLICATION_OCTET_STREAM, r.getMediaType().toString());
    }

    @Test
    public void downloadWrongFormatTest() {
        Gson gson = new Gson();

        String link = "https://en.wikipedia.org/wiki/Personal_computer";

        JsonObject j = new JsonObject();
        var links = new JsonArray();
        links.add(link);
        j.add("links", links);
        j.addProperty("format", ".docx");

        Document d = HibernateManager.getDocumentDao().create(j);
        HistoryEntry he1 = HibernateManager.getHistoryDao().create(null);
        he1.setTitle("test");
        HibernateManager.getHistoryDao().addDocument(he1, this.user, d);

        j = new JsonObject();
        j.addProperty("format", "TXT");

        var r= target("history/" + he1.getId() + "/download")
                .request()
                .header("Authorization", "Bearer " + this.accessToken)
                .post(Entity.entity(gson.toJson(j), MediaType.APPLICATION_JSON));
        Assert.assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), r.getStatus());
    }

    @Test
    public void downloadWrongHistoryIdTest() {
        Gson gson = new Gson();

        String link = "https://en.wikipedia.org/wiki/Personal_computer";

        JsonObject j = new JsonObject();
        var links = new JsonArray();
        links.add(link);
        j.add("links", links);
        j.addProperty("format", ".docx");

        Document d = HibernateManager.getDocumentDao().create(j);
        HistoryEntry he1 = HibernateManager.getHistoryDao().create(null);
        he1.setTitle("test");
        HibernateManager.getHistoryDao().addDocument(he1, this.user, d);

        j = new JsonObject();
        j.addProperty("format", "DOCX");

        var r= target("history/" + 999 + "/download")
                .request()
                .header("Authorization", "Bearer " + this.accessToken)
                .post(Entity.entity(gson.toJson(j), MediaType.APPLICATION_JSON));
        Assert.assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), r.getStatus());
    }

    @Test
    public void getFavouriteTest() {
        var he = HibernateManager.getHistoryDao().create(null);
        he.setFavourite(true);
        HibernateManager.getHistoryDao().addDocument(he, this.user, null);

        final List<HistoryEntry> historyEntryList = target("history/favourite").request()
                .header("Authorization", "Bearer " + this.accessToken)
                .get(List.class);
        Assert.assertEquals(1, historyEntryList.size());
    }

    @Test
    public void createTest() {
        Gson gson = new Gson();

        JsonObject j = new JsonObject();
        j.addProperty("title", "Test");
        j.addProperty("description", "no");
        j.addProperty("format", ".docx");
        j.addProperty("color", "#FFFFFF");

        String link = "https://en.wikipedia.org/wiki/Personal_computer";

        var links = new JsonArray();
        links.add(link);
        j.add("links", links);

        String jsonBody = target("history/add")
                .request()
                .header("Authorization", "Bearer " + this.accessToken)
                .post(Entity.entity(gson.toJson(j), MediaType.APPLICATION_JSON), String.class);

        Assert.assertTrue(JsonParser.parseString(jsonBody).getAsJsonObject().has("documentId"));
        Assert.assertTrue(JsonParser.parseString(jsonBody).getAsJsonObject().has("historyEntryId"));
    }

    @Test
    public void createFailTest() {
        Gson gson = new Gson();

        JsonObject j = new JsonObject();
        j.addProperty("title", "Test");
        j.addProperty("description", "no");
        j.addProperty("format", "FAIL_FORMAT");
        j.addProperty("color", "#FFFFFF");

        String link = "https://en.wikipedia.org/wiki/Personal_computer";

        var links = new JsonArray();
        links.add(link);
        j.add("links", links);

        Assert.assertEquals(Response.Status.BAD_REQUEST.getStatusCode(),
                target("history/add")
                .request()
                .header("Authorization", "Bearer " + this.accessToken)
                .post(Entity.entity(gson.toJson(j), MediaType.APPLICATION_JSON)).getStatus());
    }

    @Test
    public void updateHistoryEntryTest() {
        Gson gson = new Gson();

        var he = HibernateManager.getHistoryDao().create(null);
        he.setTitle("FirstTest");
        HibernateManager.getHistoryDao().addDocument(he, this.user, null);

        JsonObject j = new JsonObject();
        j.addProperty("title", "SecondTest");

        target("history/" + he.getId()).request()
                .header("Authorization", "Bearer " + this.accessToken)
                .build("PATCH", Entity.entity(gson.toJson(j), MediaType.APPLICATION_JSON))
                .property(HttpUrlConnectorProvider.SET_METHOD_WORKAROUND, true)
                .invoke();

        Assert.assertNotEquals("FirstTest", HibernateManager.getHistoryDao()
                .get(he.getId()).getTitle()
        );

        Assert.assertEquals("SecondTest", HibernateManager.getHistoryDao()
                .get(he.getId()).getTitle()
        );
    }

    @Test
    public void updateHistoryEntryFailTest() {
        Gson gson = new Gson();

        JsonObject j = new JsonObject();
        j.addProperty("title", "SecondTest");

        Assert.assertEquals(Response.Status.NOT_FOUND.getStatusCode(), target("history/999").request()
                .header("Authorization", "Bearer " + this.accessToken)
                .build("PATCH", Entity.entity(gson.toJson(j), MediaType.APPLICATION_JSON))
                .property(HttpUrlConnectorProvider.SET_METHOD_WORKAROUND, true)
                .invoke().getStatus());
    }

    @Test
    public void deleteHistoryEntryTest() {
        var he = HibernateManager.getHistoryDao().create(null);
        HibernateManager.getHistoryDao().addDocument(he, this.user, null);

        Assert.assertEquals(Response.Status.OK.getStatusCode(), target("history/" + he.getId()).request()
                .header("Authorization", "Bearer " + this.accessToken)
                .delete().getStatus());

        Assert.assertEquals(Response.Status.NO_CONTENT.getStatusCode(), target("history/" + he.getId()).request()
                .header("Authorization", "Bearer " + this.accessToken)
                .delete().getStatus());
    }
}