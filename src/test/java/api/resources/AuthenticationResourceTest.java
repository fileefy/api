package api.resources;

import api.configs.Config;
import api.configs.CustomApplicationConfig;
import api.database.HibernateManager;
import api.model.User;
import api.security.TokenManager;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class AuthenticationResourceTest extends JerseyTest {

    private final String email = "test@email.com";
    private final String password = "pass";
    private final String firstName = "TestName";
    private final String lastName = "TestLastName";
    private String accessToken;
    private User user;

    @Override
    protected Application configure() {
        return new ResourceConfig(new CustomApplicationConfig());
    }

    @BeforeClass
    public static void initMe() {
        Config.IS_TESTING = true;
        Config.configure();
    }

    @Before
    public void setUpChild() {
        HibernateManager.execute((session -> {
            session.createNativeQuery("truncate table history_entry restart identity cascade")
                    .executeUpdate();
            session.createNativeQuery("truncate table document restart identity cascade")
                    .executeUpdate();
            session.createNativeQuery("truncate table refresh_tokens restart identity cascade")
                    .executeUpdate();
            session.createNativeQuery("truncate table users restart identity cascade")
                    .executeUpdate();
            session.createNativeQuery("truncate table kindle restart identity cascade")
                    .executeUpdate();
            return null;
        }));

        // Create a new user
        JsonObject j = new JsonObject();
        j.addProperty("email", email);
        j.addProperty("password", password);
        j.addProperty("firstName", firstName);
        j.addProperty("lastName", lastName);

        this.user = HibernateManager.getUserDao().create(j);

        this.accessToken = TokenManager.issueTokens(user.getId(), false).accessToken;
    }

    @Test
    public void verify() {
        Assert.assertEquals(Response.Status.OK.getStatusCode(), target("authentication").request()
                .header("Authorization", "Bearer " + this.accessToken)
                .get().getStatus());
    }

    @Test
    public void authenticateUser() {
        Gson g = new Gson();

        JsonObject j = new JsonObject();
        j.addProperty("email", email);
        j.addProperty("password", password);
        j.addProperty("isRememberMe", false);

        String jsonBody = target("authentication").request()
                .header("Authorization", "Bearer " + this.accessToken)
                .post(Entity.entity(g.toJson(j), MediaType.APPLICATION_JSON), String.class);

        JsonObject json = JsonParser.parseString(jsonBody).getAsJsonObject();
        Assert.assertEquals(user.getId(), TokenManager.verifyAccessToken(json.get("token").getAsString()));
    }

    @Test
    public void authenticateFailUser() {
        Gson g = new Gson();

        JsonObject j = new JsonObject();
        j.addProperty("email", email + "Fail");
        j.addProperty("password", password);
        j.addProperty("isRememberMe", false);

        Assert.assertEquals(Response.Status.UNAUTHORIZED.getStatusCode(), target("authentication").request()
                .header("Authorization", "Bearer " + this.accessToken)
                .post(Entity.entity(g.toJson(j), MediaType.APPLICATION_JSON)).getStatus());
    }

    @Test
    public void refreshTest() {
        Gson g = new Gson();

        JsonObject j = new JsonObject();
        j.addProperty("email", email);
        j.addProperty("password", password);
        j.addProperty("isRememberMe", false);

        var jsonBody = target("authentication").request()
                .header("Authorization", "Bearer " + this.accessToken)
                .post(Entity.entity(g.toJson(j), MediaType.APPLICATION_JSON)).getCookies();

        Cookie c1 = jsonBody.get("refreshToken");

        jsonBody = target("authentication/refresh").request()
                .header("Authorization", "Bearer " + this.accessToken)
                .cookie(c1)
                .post(null).getCookies();

        Cookie c2 = jsonBody.get("refreshToken");
        Assert.assertNotEquals(c1, c2);
        Assert.assertEquals(1, HibernateManager.getRefreshTokenDao().get(c2.getValue()).getRotatedTokens().size());
    }

    @Test
    public void refreshFailTest() {
        Assert.assertEquals(Response.Status.UNAUTHORIZED.getStatusCode(), target("authentication/refresh").request()
                .header("Authorization", "Bearer " + this.accessToken)
                .post(null).getStatus());
    }

    @Test
    public void logoutTest() {
        Gson g = new Gson();

        JsonObject j = new JsonObject();
        j.addProperty("email", email);
        j.addProperty("password", password);
        j.addProperty("isRememberMe", false);

        var jsonBody = target("authentication").request()
                .header("Authorization", "Bearer " + this.accessToken)
                .post(Entity.entity(g.toJson(j), MediaType.APPLICATION_JSON)).getCookies();

        Cookie c = jsonBody.get("refreshToken");

        Assert.assertEquals(Response.Status.OK.getStatusCode(), target("authentication").request()
                .header("Authorization", "Bearer " + this.accessToken)
                .cookie(c)
                .delete().getStatus());
    }

    @Test
    public void logoutFailTest() {
        var a = target("authentication").request()
                .header("Authorization", "Bearer " + this.accessToken)
                .delete().getStatus();
        Assert.assertEquals(Response.Status.UNAUTHORIZED.getStatusCode(),a);
    }
}