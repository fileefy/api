package api.utilities;

import org.junit.Assert;
import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.*;

public class RandomStringTest {

    @Test
    public void nextString() {
        int length = 15;
        RandomString rs = new RandomString(length);
        Assert.assertEquals(length, rs.nextString().length());
    }

    @Test
    public void basicConstructorTest() {
        RandomString rs = new RandomString();
        Assert.assertEquals(21, rs.nextString().length());
    }

    @Test(expected = IllegalArgumentException.class)
    public void invalidConstructorTest() {
        RandomString rs = new RandomString(0, new Random(), "abcde");
    }

    @Test(expected = IllegalArgumentException.class)
    public void invalidConstructorSymbolsTest() {
        RandomString rs = new RandomString(20, new Random(), "a");
    }
}