package api.utilities.converter;

import api.configs.Config;
import api.model.Document;
import api.utilities.FileFormat;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

public class ConverterTest {

    @BeforeClass
    public static void initMe() {
        Config.IS_TESTING = true;
        Config.configure();
    }

    @Test
    public void convertTest() throws IOException, InterruptedException {
        String link = "https://en.wikipedia.org/wiki/Personal_computer";

        ArrayList<String> links = new ArrayList<>();
        links.add(link);

        Document d = Converter.convert(links, FileFormat.DOCX, false);
        Assert.assertNotNull(d.getMarkdownText());
    }

    @Test
    public void convertToFormatTest() throws IOException, InterruptedException {
        String link = "https://en.wikipedia.org/wiki/Personal_computer";

        ArrayList<String> links = new ArrayList<>();
        links.add(link);

        byte[] text = Converter.convertTo(FileFormat.DOCX, "test", "Test text");
        Assert.assertNotEquals(0, text.length);
    }
}