package api.utilities;

import org.junit.Assert;
import org.junit.Test;

public class FileFormatTest {

    @Test
    public void getFormatTest() {
        Assert.assertEquals(".docx", FileFormat.DOCX.getFormat());
    }

    @Test
    public void getTest() {
        Assert.assertEquals(FileFormat.DOCX, FileFormat.get(".docx"));
    }

    @Test
    public void valuesTest() {
        Assert.assertEquals(4, FileFormat.values().length);
    }

    @Test
    public void valueOfTest() {
        Assert.assertEquals(FileFormat.DOCX, FileFormat.valueOf("DOCX"));
    }
}