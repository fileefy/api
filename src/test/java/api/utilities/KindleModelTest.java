package api.utilities;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class KindleModelTest {

    @Test
    public void getModelTest() {
        Assert.assertEquals("Kindle 9", KindleModel.KINDLE_9.getModel());
    }

    @Test
    public void getTest() {
        Assert.assertEquals(KindleModel.KINDLE_9, KindleModel.get("Kindle 9"));
    }

    @Test
    public void valuesTest() {
        Assert.assertEquals(4, KindleModel.values().length);
    }

    @Test
    public void valueOfTest() {
        Assert.assertEquals(KindleModel.KINDLE_9, KindleModel.valueOf("KINDLE_9"));
    }
}