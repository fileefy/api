package api.utilities;

import api.model.User;
import club.caliope.udc.OutputFormat;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class UtilitiesTest {

    @Test
    public void splitCamelCaseTest() {
        String camelCase = "camelCaseText";
        Assert.assertEquals(3, Utilities.splitCamelCase(camelCase).length);
    }

    @Test
    public void generateErrorTest() {
        String message = "It is error";
        String error= Utilities.generateError(message);

        JsonObject json = JsonParser.parseString(error).getAsJsonObject();

        Assert.assertTrue(json.has("error"));
        Assert.assertEquals(message, json.get("error").getAsString());
    }

    @Test
    public void generateErrorResponseTest() {
        Map<String, String> errors = new HashMap<String, String>();
        String message = "Error in field";

        errors.put("field", message);
        String error = Utilities.generateError(errors);

        JsonObject json = JsonParser.parseString(error).getAsJsonObject();

        Assert.assertTrue(json.has("errors"));
        Assert.assertEquals(1, json.get("errors").getAsJsonObject().size());
        Assert.assertEquals(message, json.get("errors").getAsJsonObject().get("field").getAsString());
    }

    @Test
    public void updateEntityTest() {
        String email = "test@test.com";
        String newEmail = "newTest@test.com";

        User u = new User(email, "password", "firstName", "lastName");

        JsonObject j = new JsonObject();
        j.addProperty("email", newEmail);

        try {
            Utilities.updateEntity(u, j);
        } catch (Exception e){
            throw new AssertionError();
        }

        Assert.assertEquals(newEmail, u.getEmail());
    }

    @Test
    public void convertFormat() {
        Assert.assertEquals(OutputFormat.DOCX, Utilities.convertFormat(FileFormat.DOCX));
        Assert.assertEquals(OutputFormat.EPUB, Utilities.convertFormat(FileFormat.EPUB));
        Assert.assertEquals(OutputFormat.FB2, Utilities.convertFormat(FileFormat.FB2));
        Assert.assertEquals(OutputFormat.MARKDOWN, Utilities.convertFormat(FileFormat.MARKDOWN));
    }
}