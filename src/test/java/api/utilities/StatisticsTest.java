package api.utilities;

import api.configs.Config;
import api.configs.CustomApplicationConfig;
import api.database.HibernateManager;
import api.model.User;
import api.security.Role;
import api.security.TokenManager;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.core.Application;

public class StatisticsTest extends JerseyTest {

    private final String email = "test@email.com";
    private final String password = "pass";
    private final String firstName = "TestName";
    private final String lastName = "TestLastName";
    private String accessToken;
    private User user;

    @Override
    protected Application configure() {
        return new ResourceConfig(new CustomApplicationConfig());
    }

    @BeforeClass
    public static void initMe() {
        Config.IS_TESTING = true;
        Config.configure();
    }

    @Before
    public void setUpChild() {
        HibernateManager.execute((session -> {
            session.createNativeQuery("truncate table history_entry restart identity cascade")
                    .executeUpdate();
            session.createNativeQuery("truncate table document restart identity cascade")
                    .executeUpdate();
            session.createNativeQuery("truncate table refresh_tokens restart identity cascade")
                    .executeUpdate();
            session.createNativeQuery("truncate table users restart identity cascade")
                    .executeUpdate();
            session.createNativeQuery("truncate table kindle restart identity cascade")
                    .executeUpdate();
            return null;
        }));

        // Create a new user
        JsonObject j = new JsonObject();
        j.addProperty("email", email);
        j.addProperty("password", password);
        j.addProperty("firstName", firstName);
        j.addProperty("lastName", lastName);

        this.user = HibernateManager.getUserDao().create(j);
        this.user.assignRole(Role.ADMIN);
        this.user = HibernateManager.getUserDao().update(this.user);

        this.accessToken = TokenManager.issueTokens(user.getId(), false).accessToken;
    }

    @Test
    public void generateStatistics() {

        String jsonBody = Statistics.generateStatistics();
        JsonObject json = JsonParser.parseString(jsonBody).getAsJsonObject();

        Assert.assertTrue(json.has("userCount"));
        Assert.assertTrue(json.has("documentCount"));
        Assert.assertTrue(json.has("usedSpace"));
        Assert.assertTrue(json.has("allocatedSpace"));
        Assert.assertTrue(json.has("serverTime"));
    }
}