package api.configs;

import org.junit.Assert;
import org.junit.Test;

public class ConfigTest {

    @Test
    public void getenvTest() {
        if (Config.ON_LOCAL) {
            Config.IS_TESTING = false;
            Config.configure();
            Assert.assertNotNull(Config.getenv("PORT"));
        }
    }

    @Test
    public void configureTest() {
        if (Config.ON_LOCAL) {

            Config.IS_TESTING = false;
            Config.configure();

            // Check if the value in converter config was assigned
            Assert.assertNotEquals(null, (ConverterConfig.ACCESS_TOKEN));
        }
    }


    @Test
    public void testConfigureTest() {
        if (Config.ON_LOCAL) {

            Config.IS_TESTING = true;
            Config.configure();

            // Check if the value in converter config was assigned
            Assert.assertNotEquals(null, (ConverterConfig.ACCESS_TOKEN));
        }
    }
}