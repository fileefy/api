## Copy Java environment
FROM openjdk:11-jdk

## Install LaTeX
RUN apt update -y
RUN apt install -y --no-install-recommends pandoc texlive-latex-recommended python python3-pip python3-setuptools

WORKDIR /app

## Copy whole project
COPY ./build/libs ./build/libs
COPY ./fileefy-converter ./fileefy-converter
COPY ./requirements.txt ./requirements.txt

RUN pip3 install -r requirements.txt

# Running command
CMD ["java", "-jar", "build/libs/fileefy-api-1.0-SNAPSHOT.jar"]