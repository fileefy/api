openapi: 3.0.0
info:
  title: Individual Project
  version: v1
tags:
  - name: userModel
    x-displayName: User Model
    description: |
      <SchemaDefinition schemaRef="#/components/schemas/User" />
  - name: documentModel
    x-displayName: Document Model
    description: |
      <SchemaDefinition schemaRef="#/components/schemas/Document" />
  - name: historyEntryModel
    x-displayName: HistoryEntry Model
    description: |
      <SchemaDefinition schemaRef="#/components/schemas/HistoryEntry" />
  - name: errorModel
    x-displayName: Error Model
    description: |
      <SchemaDefinition schemaRef="#/components/schemas/Error" />
x-tagGroups:
  - name: Models
    tags: 
      - userModel
      - documentModel
      - historyEntryModel
      - errorModel
  - name: API
    tags:
      - Users
      - Logging In
      - History
      - Documents
servers:
  - url: 'https://localhost:9090/api/'
security:
   - Bearer: []
paths:
  /hello:
    get:          
      summary: 'printHello'
      operationId: hello
      security: []
      tags:
        - Test
      responses:
        '200':
          description: 'Successful operation'
          content:
            application/json:
              schema:
                type: object
                properties:
                  text:
                    type: string
        '401':
          $ref: '#/components/responses/Unauthorized'
  /users:
    get:          
      summary: 'Get a list of users'
      operationId: getUsers
      tags:
        - Users
      responses:
        '200':
          description: 'Successful operation'
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/User'
        '401':
          $ref: '#/components/responses/Unauthorized'
    post:
      summary: 'Create a new user'
      operationId: createUser
      security: []
      tags:
        - Users
      requestBody:
        description: Email, username and password are **required**.
        required: true
        content:          
          application/x-www-form-urlencoded:
            schema:
              type: object
              properties:
                email:
                  type: string
                  format: email
                username:
                  type: string
                password:
                  type: string
                  format: password      
                firstName:
                  type: string
                lastName:
                  type: string
              required:
                - email
                - username
                - password        
      responses:
        '200':
          description: 'User created'        
        '401':
          $ref: '#/components/responses/Unauthorized'
  /users/{id}:
    get:
      summary: 'Get a user by Id'
      operationId: getUserById
      tags:
        - Users
      parameters:
        - $ref: '#/components/parameters/id'
      responses:
        '200':
          description: 'Successful operation'
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
        '401':
          $ref: '#/components/responses/Unauthorized'
    patch:
      summary: 'Update user details'
      operationId: updateUser
      tags:
        - Users
      parameters:
        - $ref: '#/components/parameters/id'
      requestBody:
        description: 'New data'
        content:
          application/x-www-form-urlencoded:
            schema:
              type: object
              properties:
                email:
                  type: string
                  format: email
                username:
                  type: string    
                firstName:
                  type: string
                lastName:
                  type: string
      responses:
        '200':
          description: 'User updated'
        '401':
          $ref: '#/components/responses/Unauthorized'
    delete:
      summary: 'Delete user'
      operationId: deleteUser
      tags:
        - Users
      parameters:
        - $ref: '#/components/parameters/id'
      responses:
        '200':
          description: 'User deleted'
        '401':
          $ref: '#/components/responses/Unauthorized'
  /users/login:
    post:
      summary: 'Log into the system'
      operationId: login
      security: []
      tags:
        - Logging In        
      requestBody:
        required: true
        content:          
          application/x-www-form-urlencoded:
            schema:
              type: object
              properties:
                username:
                  type: string
                password:
                  type: string
                  format: password
              required:
                - username
                - password
      responses:
        '200':
          description: 'Logged in'
  /users/logout:
    post:
      summary: 'Log out from the system'
      operationId: logout
      tags:
        - Logging In
      responses:
        '200':
          description: 'Logged out successfully'
        '401':
          $ref: '#/components/responses/Unauthorized'
  /users/{id}/history:
    get:
      summary: 'List all entries in the history'
      operationId: getHistoryById      
      parameters:
        - $ref: '#/components/parameters/id'
      tags:
        - History
      responses:
        '200':
          description: 'History send'
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/HistoryEntry'   
        '401':
          $ref: '#/components/responses/Unauthorized' 
    post:
      summary: 'Create a new history entry'
      operationId: createHistoryEntry
      parameters:
        - $ref: '#/components/parameters/id'
      tags:
        - History
      requestBody:
        required: true
        content:
          application/x-www-form-urlencoded:
            schema:
              type: object
              properties:
                documentId:
                  type: integer
                  format: int64
                title:
                  type: string
              required:
                - documentId
      responses:
        '200':
          description: 'Entry created'        
        '401':
          $ref: '#/components/responses/Unauthorized'
  /users/{id}/history/{entry}:
    get:
      summary: 'Get history entry by Id'
      operationId: getHistoryEntryById      
      parameters:
        - $ref: '#/components/parameters/id'
        - $ref: '#/components/parameters/entry'
      tags:
        - History
      responses:
        '200':
          description: 'History entry sent'
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/HistoryEntry'   
        '401':
          $ref: '#/components/responses/Unauthorized' 
    patch:      
      summary: 'Update history entry by Id'
      operationId: updateHistoryEntryById      
      parameters:
        - $ref: '#/components/parameters/id'
        - $ref: '#/components/parameters/entry'
      tags:
        - History
      requestBody:
        description: 'New title'
        content:
          application/x-www-form-urlencoded:
            schema:
              type: object
              properties:
                title:
                  type: string
      responses:
        '200':
          description: 'History entry updated'
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/HistoryEntry'   
        '401':
          $ref: '#/components/responses/Unauthorized' 
    delete:      
      summary: 'Delete history entry by Id'
      operationId: deleteHistoryEntryById      
      parameters:
        - $ref: '#/components/parameters/id'
        - $ref: '#/components/parameters/entry'
      tags:
        - History
      responses:
        '200':
          description: 'History entry deleted'
        '401':
          $ref: '#/components/responses/Unauthorized' 
  /documents:
    get:
      summary: 'List all document of the user'
      operationId: getAllDocuments
      tags:
        - Documents
      responses:
        '200':
          description: 'Documents listed'
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Document' 
        '401':
          $ref: '#/components/responses/Unauthorized'
    post:
      summary: 'Creates a new document'
      operationId: createDocument
      tags:
        - Documents
      requestBody:
        description: 'Create a new document'
        content:
          application/x-www-form-urlencoded:
            schema:
              type: object
              properties:
                url:
                  type: string
                  format: url
                format:
                  $ref: '#/components/schemas/Formats'
              required:
                - url
                - format             
      responses:
        '200':
          description: 'Document created'
        '401':
          $ref: '#/components/responses/Unauthorized'
  /documents/{id}:
    get:
      summary: 'Get document by Id'
      operationId: getDocumentById      
      parameters:
        - $ref: '#/components/parameters/id'
      tags:
        - Documents
      responses:
        '200':
          description: 'Document sent'
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Document'   
        '401':
          $ref: '#/components/responses/Unauthorized' 
    patch:      
      summary: 'Update document by Id'
      operationId: updateDocumentById      
      parameters:
        - $ref: '#/components/parameters/id'
      tags:
        - Documents
      requestBody:
        content:
          application/x-www-form-urlencoded:
            schema:
              type: object
              properties:
                title:
                  type: string
                file:
                  type: string
                  format: base64
      responses:
        '200':
          description: 'Document updated'
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Document'   
        '401':
          $ref: '#/components/responses/Unauthorized' 
    delete:      
      summary: 'Delete document by Id'
      operationId: deleteDocumentById      
      parameters:
        - $ref: '#/components/parameters/id'
      tags:
        - Documents
      responses:
        '200':
          description: 'Document deleted'
        '401':
          $ref: '#/components/responses/Unauthorized' 
components:
  schemas:
    User:
      type: object
      properties:
        id:
          type: integer
          format: int64
        username:
          type: string
        email:
          type: string
          format: email
        firstName:
          type: string
        lastName:
          type: string
        created-at: 
          type: string
          format: date-time
        updated-at:
          type: string
          format: date-time
        deleted-at:
          type: string        
          format: date-time
    Document:
      type: object
      properties:
        id:
          type: integer
          format: int64
        ownerId:
          type: integer
          format: int64
        sourceUrl:
          type: string
          format: url
        title:
          type: string
        format:
          type: string
        file:
          type: string
          format: base64
        created-at: 
          type: string
          format: date-time
        updated-at:
          type: string
          format: date-time
        deleted-at:
          type: string        
          format: date-time
    Error:
      type: object
      properties:
        error:
          type: string
    HistoryEntry:
      type: object
      properties:
        id:
          type: integer
          format: int64
        title:
          type: string
        documentId:
          type: integer
          format: int64
    Formats:
      type: string
      enum:
        - mobi
        - docx
        - pdf
        - epub
  securitySchemes:
    Bearer:
      type: apiKey
      description: Token returned after user gets authenticated.
      name: authorization
      in: header
  responses:
    NotFound:
      description: The specified resource was not found
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Error'
    Unauthorized:
      description: Unauthorized
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Error'
  parameters:
    id:
      in: path
      name: id
      required: true
      schema:
        type: integer
        format: int64
      description: Element Id
    entry:
      in: path
      name: entry
      required: true
      schema:
        type: integer
        format: int64
      description: History entry Id
  examples: {}
  requestBodies: {}
  headers: {}
  links: {}
  callbacks: {}